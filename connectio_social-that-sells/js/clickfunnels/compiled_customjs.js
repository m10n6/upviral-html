
/*================ START : CLICK TRIGGER FOR WEBINARJAM ===============  */
$(document).ready(function(){
	var btn1 = 'tmp_button-54776';
	$('#' + btn1).click(function(e){
		e.stopPropagation();
      setTimeout(function(){
        console.log('Trigger CLick - MC');
        $('#mc-btn-1368810').trigger('click');
      }, 100);
		$('#webjamBtn').trigger('click');
	});
	var btn2 = 'tmp_button-73389';
	$('#' + btn2).click(function(e){
		e.stopPropagation();
      setTimeout(function(){
        console.log('Trigger CLick - MC');
        $('#mc-btn-1368810').trigger('click');
      }, 100);
		$('#webjamBtn').trigger('click');
	});
	var btn3 = 'button-95074';
	$('#' + btn3).click(function(e){
		e.stopPropagation();
      setTimeout(function(){
        console.log('Trigger CLick - MC');
        $('#mc-btn-1368810').trigger('click');
      }, 100);
		$('#webjamBtn').trigger('click');
	});	
});

/*================ END : CLICK TRIGGER FOR WEBINARJAM ===============  */

/*================ START : DISPLAY SCHEDULE ===============  */
function ordinal_suffix_of(i) {
    var j = i % 10,
        k = i % 100;
    if (j == 1 && k != 11) {
        return i + "st";
    }
    if (j == 2 && k != 12) {
        return i + "nd";
    }
    if (j == 3 && k != 13) {
        return i + "rd";
    }
    return i + "th";
}
var dnowDisp = 'tmp_featureimage-43939';
var monthWord = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
var dayWord = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
var now = new Date();
var newDnowDisp = dayWord[now.getDay()] + ', ' + monthWord[now.getMonth()] + ' ' + ordinal_suffix_of(now.getDate());
$('#' + dnowDisp + ' h3 ').html(newDnowDisp);
/*================ END : DISPLAY SCHEDULE ===============  */

/*================ START : DISPLAY COUNTDOWN TIMER ===============  */
var timerID = 'tmp_countdown_evergreen-47602';
var now = new Date();
//var dminutes = 59 - now.getMinutes();
  
var dminutes = (Math.ceil(now.getMinutes() / 15) * 15) - now.getMinutes() ;//- now.getMinutes()
console.log('new minutes = ' + dminutes); 
var dsecs = 59 - now.getSeconds();
$('#' + timerID + ' div').attr('data-minutes', dminutes);	
$('#' + timerID + ' div').attr('data-seconds', dsecs);
  
var xmins = 'tmp_headline1-85415';
var xm = $('#'+ xmins + ' > div > b').html();
$('#'+ xmins + ' > div > b').html( xm.replace("{x}", '<span id="sp-'+ xmins +'">' + Number(dminutes) + '</span>') );

setInterval(function(){
	console.log('val => ' + $('#' + timerID + ' > div > span > span:nth-child(2) > span:nth-child(1)').html());
	$('#sp-' + xmins ).html($('#' + timerID + ' > div > span > span:nth-child(2) > span:nth-child(1)').html());
}, 500);
  
var dnowDisp = 'tmp_featureimage-43939';
//var mom = moment().tz("America/Los_Angeles").add(1, 'hour').startOf('hour').format('Ha');
//var mom1 = moment().tz("America/New_York").add(1, 'hour').startOf('hour').format('Ha');
var mom = moment().tz("America/Los_Angeles").add(1, 'hour').startOf('hour').format('H');
var mom1 = moment().tz("America/New_York").add(1, 'hour').startOf('hour').format('H');
  
var plus15 = (Math.ceil(now.getMinutes() / 15) * 15);
if(plus15 == 60){
  plus15 = '00';
}

var am_pm = 'AM';
if(mom > 11){
    am_pm = 'PM';
}

var am_pm1 = 'AM';
if(mom1 > 11){
    am_pm1 = 'PM';
}
//$('#' + dnowDisp + ' b ').html(mom + ' Pacific Time , ' + mom1 + ' Eastern Time');
  $('#' + dnowDisp + ' b ').html(mom + ':' + plus15 + ' ' + am_pm +' Pacific Time , ' + mom1 + ':' + plus15 + ' ' + am_pm1 + ' Eastern Time');
/*================ END : DISPLAY COUNTDOWN TIMER ===============  */