<?php
include('config.php');
if(!empty($emailreg)){
  // sleep(10);
  // date_default_timezone_set('America/New_York');
  $schedule = $_POST['webinar_dt'];

  $ch = curl_init();
  $header = array("User-Agent: " . $_SERVER["HTTP_USER_AGENT"], "Content-Type: application/x-www-form-urlencoded");

  $todayreg = date("Y-m-d");


  curl_setopt($ch, CURLOPT_URL,"https://webinarjam.genndi.com/api/everwebinar/register");
  curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_POST, 1);

  // curl_setopt($ch, CURLOPT_POSTFIELDS,
  //             "api_key=".$api_key."&webinar_id=".$webinar_id."&first_name=".$namereg."&email=".$emailreg."&schedule=".$schedule."&real_dates=1");
  curl_setopt($ch, CURLOPT_POSTFIELDS,
              "api_key=".$api_key."&webinar_id=".$webinar_id."&first_name=".$namereg."&email=".$emailreg."&real_dates=1&schedule=".$schedule);//&selected_date=".$todayreg."&to_timezone=America/New_York&from_timezone=America/New_York&tz_selected=1");
  // curl_setopt($ch, CURLOPT_POSTFIELDS,
  //             "webicode=".$webicode."&memberid=".$memberid."&page=registration&page_tag=formregistration&userid=0&first_name=".$namereg."&email_address=".$emailreg."&selected_date=".$todayreg."&selected_schedule=jot&to_timezone=America/New_York&from_timezone=America/New_York&tz_selected=1");
        
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

  $server_output = curl_exec ($ch);


  $pieces = explode(" ", $server_output);
  $pieces2 = explode("'", $server_output);

  // echo '<pre>';
  // print_r($pieces2);
  // // echo $schedule;
  // echo '</pre>';
  $result = json_decode($pieces2[0]);
  // print_r($result->user->live_room_url);

  $URLGO = $result->user->thank_you_url;
  // $URLGO = $pieces2[7];
  // echo $URLGO;
  curl_close ($ch);

  // echo $emailreg;
  ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');


  ga('create', 'UA-62842650-1', 'auto', {'allowLinker': true});
  ga('require', 'linker');
  ga('linker:autoLink', ['connectio.io','zaxaa.com', 'emarky.zaxaa.com', 'app.webinarjam.net', 'emarky.thrivecart.com'], true);
  
  
  ga('send', 'pageview');


  // window.location='https://app.upviral.com/login.php';
  // var destinationLink = 'https://app.upviral.com/login.php';
  // var x = ga('linker:decorate', destinationLink);
  // alert(x);

  ga(function(tracker) {
    var linkerParam = tracker.get('linkerParam');
    //alert(linkerParam);
    // var destinationLink = 'https://app.upviral.com/login.php?' + linkerParam;
    // var destinationLink = 'https://emarky.zaxaa.com/o/2943330132929/5#'+ linkerParam;
    var destinationLink = '<?php echo $URLGO; ?>&'+ linkerParam;
    window.location = destinationLink;
  });
</script>
<?php
  // echo "<script type='text/javascript'>document.location.href='{$URLGO}';</script>";
  // echo '<META HTTP-EQUIV="refresh" content="0;URL=' . $URLGO . '">';

}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>UpViral Signup Template</title>

    <!-- Bootstrap -->
    <link href="js/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700,800|Poppins:400,700" rel="stylesheet">
    <link href="css/custom.css?v=0.0.55" rel="stylesheet">
    <script src="js/moment.js"></script>
    <script src="js/moment-timezone-all.js"></script>
  </head>
  <body>

    <div class="container-fluid">

      <div class="row">

        <div id="header" class="col-md-12">

          <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
              <!-- style="color: #fff; margin: 50px 10px 20px 25%;" -->
              <div class="col-md-10 pull-right" id="header-left" style="">
                <span class="font22px"><strong>FREE</strong> Training by Wilco de Kreij</span>
                <h2 class="fontwhite"><strong>How To Get 16,435<br>Free Subscribers<br>Within 30 Days<br/>Or Less...</strong></h2>
                <span class="font24px">... without relying on <strong>ANY</strong> of your<br>current traffic sources</span>
                <div id="timerwrap" style="">
                    <div class="" id="hours" style="">
                        <span style="">12</span>
                        <p style="">HOUR</p>
                    </div>
                    <div class="timerdivider" style="">
                        :
                    </div>
                    <div class="" id="minutes" style="">
                        <span style="">12</span>
                        <p style="">MINUTE</p>
                    </div>
                    <div class="timerdivider" style="">
                        :
                    </div>
                    <div class="" id="seconds" style="">
                        <span style="">12</span>
                        <p style="">SECOND</p>
                    </div>
                </div>
              </div>
            </div>

            <div class="col-md-6 col-sm-12 col-xs-12">
              <div id="header-right" style="">
              </div>
            </div>

            <div style="clear: both"></div>

            <div class="col-md-12 text-center" id="divClaim">
              <a href="#" class="btn btn-extra-lg btn-orange btn-claimspot"><strong>CLAIM YOUR SPOT NOW</strong><br>Very Limited Seating... Register Now!</a>
            </div>

          </div>
          <!-- .row -->

          <div style="clear: both"></div>
          <div class="text-left" style="" id="giftmain">
              <div class="1col-md-5 gift_arrow" style="" >
                <div class="gift_icon float_left" style=""></div>
                <p style="">Get our upcoming unreleased software for <span class="green_text tab_fontwhite"><strong>FREE</strong></span><br>
                (no catch) by showing up to the call!</p>
              </div>
          </div>

          <div style="clear: both"></div>
          <div class="clearfix spacer40"></div>

    <!--       <div class="row" style="background:#ccc;">
            <div class="col-md-12">
              <div class="col-md-4 col-md-offset-1" style="background:red;">
                <div class="row">
                  <div id="calendar1" class="float_left"></div>
                  <div id="calendar_text" class="float_left">
                      <h3>Wednesday, May 31st</h3>
                      <span>6am Pacific Time, 9am Eastern Time</span>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-md-offset-2" style="background:blue">

                asdasd
              </div>
            </div>
          </div> -->
          <div class="clearfix spacer50_mobile"></div>
          <div class="row no-margin-lr" >

            <div class="col-md-6 col-sm-6 col-xs-12 infobox_main">
              <div class="col-md-10 pull-right infobox_wrapper">
                <div class="row">
                  <div class="col-md-12">

                    <div class="col-md-12 box_shadow infobox" style="">
                    
                      <div id="calendar1" class="float_left"></div>
                      <div id="calendar_text" class="float_left info_boxtext">
                          <h3 class="fontw700 font22px">Wednesday, May 31st</h3>
                          <span>6am Pacific Time, 9am Eastern Time</span>
                      </div>
                      <div style="clear: both;"></div>
                    </div>
                    <div style="clear: both;"></div>

                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-6  col-sm-6 col-xs-12 infobox_main">
              <div class="col-md-10  infobox_wrapper">
                <div class="row">
                  <div class="col-md-12">

                    <div class="col-md-12 box_shadow infobox" style="">
                    
                      <div id="wilco_present" class="float_left"></div>
                      <div id="present_text" class="float_left info_boxtext">
                          <h3 class="fontw700 font22px">Presented by Wilco de Kreij</h3>
                          <span>Only 200 Seats Available!</span>
                      </div>
                      <div style="clear: both;"></div>
                    </div>
                    <div style="clear: both;"></div>

                  </div>
                </div>
              </div>
            </div>

          </div>
          <!-- .row -->

        </div>
        <!-- #header -->

      </div>
      <!-- .row -->

      <div id="body">
        <div class="row">
          <div class="col-md-12 text-center">
              <h2 class="fontw700">What You'll Learn On This <span class="green_text">FREE</span> Training:</h2>
          </div>
        </div>

        <div class="spacer20"></div>



      </div>
      <!-- #body -->

      <div class="spacer20"></div>

      <div class="row">
        <div id="footer">

            <div class="row no-margin-lr">
              <div class="col-md-10 col-md-offset-1">

                <div class="row">
                  <div class="col-md-4 text-center secretbox" style="">
                    <div class="secret_icon center-block"></div>
                    <div class="spacer20"></div>
                    <h3 class="fontw700 poppins">Secret #1:</h3>
                    An unusual, yet powerful traffic strategy that's not depending on any of the "usual suspects" (like Google, Facebook, etc).
                    <div class="secret1_arrow" style=""></div>
                    <div style="clear: both;"></div>
                  </div>

                  <div class="col-md-4 text-center secretbox" style="">
                    <div class="secret_icon center-block"></div>
                    <div class="spacer20"></div>
                    <h3 class="fontw700 poppins">Secret #2:</h3>
                    How to get a massive audience at your disposal from day 1... without having to pay for it.
                    <div class="secret2_arrow" style=""></div>
                    <div style="clear: both;"></div>
                  </div>

                  <div class="col-md-4 text-center secretbox" style="">
                    <div class="secret_icon center-block"></div>
                    <div class="spacer20"></div>
                    <h3 class="fontw700 poppins">Secret #3:</h3>
                    How to 10X the amount of leads you're getting from the same amount of traffic.
                    <div class="spacer30"></div>
                    <div class="secret3_arrow" style=""></div>
                    <div style="clear: both;"></div>
                  </div>
                </div>
              </div>
            </div>

            <div class="row  no-margin-lr">
              <div class="col-md-12 text-center">
                <div class="clearfix spacer20_1024px"></div>
                <a href="#" class="btn btn-extra-lg btn-orange btn-noborder" id="btnregister-2">
                    <strong >REGISTER MY SEAT NOW!</strong><br>Very Limited Seating... Register Now!</a>
                </a>
                <div class="spacer30"></div>
                <p class="font20px">You'll get our upcoming unreleased software <strong class="green_text">FOR FREE</strong> (no catch)<br>just for showing up!</p>
              </div>    
            </div>

            <div class="spacer50"></div>

            <div class="row  no-margin-lr">
                <div class="col-md-8 col-md-offset-2 testimon" style="padding: 0px !important;">
                  <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin feugiat nisl maximus enim malesuada, vel viverra quam rhoncus. Ut sed tincidunt dui, a varius mauris. Sed accumsan bibendum ex, sed sollicitudin orci sollicitudin sit amet. Fusce malesuada, tortor ut accumsan congue, nunc quam vulputate nisl, sit amet consectetur justo mauris at arcu.</p> -->

                  <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                      <li data-target="#myCarousel" data-slide-to="1"></li>
                      <li data-target="#myCarousel" data-slide-to="2"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                      <div class="item active">
                        <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin feugiat nisl maximus enim malesuada, vel viverra quam rhoncus. Ut sed tincidunt dui, a varius mauris."</p>
                        <div class="spacer20"></div>
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEgAAABICAYAAABV7bNHAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6ODdGQUNGNTk1QTkxMTFFN0EzMTNDNzA5NTUxQjhCNTUiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6ODdGQUNGNUE1QTkxMTFFN0EzMTNDNzA5NTUxQjhCNTUiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo4N0ZBQ0Y1NzVBOTExMUU3QTMxM0M3MDk1NTFCOEI1NSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo4N0ZBQ0Y1ODVBOTExMUU3QTMxM0M3MDk1NTFCOEI1NSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PlOirJMAADBjSURBVHjadHxpsGxndd06Y5+ehzvfd988SHrS04wmBAYxGBuMjcuQchLKJNhxnKqQcjmuyg87lcpUKaBSUMFgTOLZSQXb4NjGwdiMRgEJJCEJvSe9ebzz7Xk+U9be53TffpBIat3uvj18Z397r73W3vu7xrs++AEMNjcw6nUQhgHiKAIM/mcYSP4FYiC5Z+z/1HvpT3mFoQ/S+/q0/h/7r9B/LN6/n8/cx8d38PEpA/EJ3p/jKwpxFBfj5Ms6/PSuYRp7hmFeNA3rvGVZr5mW+aJpm9+zTDs0bJtfYcLNZ1GoLSBbmYOZyaG7t4dLr3wHUTjGyYfeiFxpHptnn8Hg0vM4vLyI0l2PoL9wAmPbg9HaRaa3g+Lhe1A7cifKWRcxFxDFEe0Qo09b2N5wCD8IEPg+Yt4QJ5eiBjKM6dUZkwvmTzO9ajFWzJ+mPohTU+A2w/D/B/nzPXzwlhjxG/mGysR+yVfF6QNwYbEukP8UTQNFGmCFt3sMM4QVmTAjC2ZsNS0z/oYZx182TfNPbN9Zt/0ATsDXOBF4icgEPkbDPjDoITN/AJXKEgaxg9HeJrzWFozSARhWBghjfZ8bBcjwKjwanCtAJKvi9ThyrWEms+8tE4NMb0bqKfu/M8QQqYclP7HvUekb+a/H28/x/tf54Brf8XH+4t38ukryXYldTFrBpieYqYFMfXec3iafN2tw/VcM/G7ePs63XOcbv8hX/QOa15PXmLxIx7YQj4cYdVtq8FxtCQ49qTsYwW/twBl1ks82zWQh9Bgjjn7om8Y0lZlcqDk1EiahZEwfqRVk0WbqNcZt4XPbRZT5ml/nz2t8ze/yZW+Ul4ghzDQkLYsX4DA8+Jy4sWVavCAn9ShZrHyimX5cvL+G1GwT70ufpyXwo3zfH/J91/jUr0eGUbYcFxG9aNjaQ0BPcgslZGvL6PmM3foOzN4uPTJEbNlJ6EtI7W8ApsghK9GLNTH1hmR5ZmqISahNjHS7OSa4w39d3n6Fd67wiX/L5xeTzzIJOuZ+6MUJVhFPkMtm4WU9eF6W9z0aiXjiOOpVURTtm39mXcY0jPcvIk4NyduiId8NXCZW/YptWq5P44y6DZj00uLCCuAV0Wm3EXeIPX6XNk3WlnjPDLSk3yK/tZO9NX8AN2ZBeNYYMw64b+Y38/6nef/kdNdTw8bp19p0Zde1kS8UkOUtn82hXC6i1++jUW+g0Wxq7Oc8F7aVRavbx3A0ZvglXmNOgDDe36DYSL1SvS5OvU283Kzxuz/qZrxfNAzrF/1B+6sxvSVHD5IwG9QvIWjvwRm2YTvl5BroyUacrDtxpQnwSshaKTAxg4lbhsFY7ydYk2zfvm1mcApGjv/7JO9/mT9PYgpb9BDT1AwjCzcZx44VcycEhSN4xIdsLotiuYpKLoNep4XecMxgcdAn2PZGQxxeXcDhA4u6DtOYhNoPbFA8u5nx5L/k9bxjue5J1/G+7I/HnwzG/ZyTL8KjkfoE+35zD1avDluyNkOcC9tHTwP7UKIexP/bbgZeqZzugIHQlywwkPf9/8LrFG9/yufv+QG307uRbigvjjeLRhr0hwjp5rxi7PhjZhAHxVIJ29s7KNgxKnN5fU9rHOL8zW002z287YkHkM/ncO7iVYahl3px/P/EvsSJ4mleSbyXm+JK5Ju/NBp03uhWCj9dWlw937nKMOOmVJnebW8eIwW/aJI9E6iJE8yVD2MScVAoVeAurMLL5xU0e509bF27iEGrpVkGM1DJf97O932Wj8uzDGeyYA0JfpmNkBcdYkQKIZ+ZIcZ0e11UmDWLBQ/1RgOVcgFHlyukGGMMhyP0r2+iQi6ysdfCMy++irc8+TDG9KrzV27yPfmZTYpTWjEL5mmopRk3CkOGEEPWznCze3fng/F3ctWl97rF+S91W5cZZvSgQgfjePL+aIp3kbFP40wBzALdvbq6huqhEygfPKGAZrmOhsQU2ZM3vJ/3viDZamrtGe+SsGLE0jjkU+QWw7Gvv8m7Flq9vhKwrGNij2QO/gDVSp4vdlCem+NO2qhlLdQ8E8vlPAYMtb975kUcWJ7HykINY2JSPJsg4tmQizH7S2KPGogXR+zzEI3HGA+6JSdb+EJ2bunnBqGJXqsOa0gAD8n/4oSUJbba/1wJNVswyDJcGsSDyYwSm0zBXLQ5QXjuUpy49i/QGJ9OYMb4oQQ/AXGDDJaME4MxySdDquxZ6NI7hH4VvQy9qE8iF2Iua6C9M4Tjuky/glMGjq4tIcPsttTtoU/OcnOvjXPnr+CO44cYajfQpZGEJggMxBMolGBS++xnIQHwQJElVAP54Yhe1EauULELiwd+p3U177Zarc8UmOGsrGBluB+raRYz0yREImBydwYw6rsJUPMFnb0djGl105y4sPFevu1TaU78oQyXmJvAzN0QAwX0vCHxpJa3NcRGQUxDmXyOXuCHyFshWls3IfBo5GuUCgYWyjkNxeOFHDq7u+h0h1icq2Cn3kGTHneQwH311k5qEAH/eJrZpheXMiWyK66BTirsmhtgBzTSqEsp5SNXXTCYzT7Va19vjduNz5p0Dln3JBGYatj0Ea/VjhkKXVpyb28D9rqlpus36xhzFwWfSOie4pf+oeioH07/CZhJaAmXiIORLr43CjSsxJbtIQ2SsXX9Qy64kOHrw4FEGEami764uRljRANmc6QAuTxCekqpTGzixS9Vi9jY7SDLlL+yWMOt7foUecypB6Xek0oiU0kolYQYiBjKlE8W3cR42IWXKyI/t2x169f/oN9t7lbyxa9Y0b7EmkgoTfviQcJme80WGtvrvMAQCesFXGYa07FO8cs/x/e4k3gybtNdaaiJp/GKTXpPL0g0lcOLbvQDdXdZbXcU6WuLrqW7M6AOktCj/3IzTOxsxpifHyMcF1EollFiBhsOfBRzOWajHEaDPsoLRew1exgxy0rymCb/KRdKcNBMJYRQFnnepYiV50f9NrL5KjH2AFpXc26n2/ncwrL/iGtF56fv/wGuJ6RZ2W6GIOkKoRMtQwpONkqeE/8pX1SepPmJXDBmmLaZGsDgYkKGlh8SiMl1JMSGDC/uAMM1RJ+YVHShIC4pvc2L7zL9510+CAVEOySNdTQbdQTkYwHDMqAnudzxtbWDBHeyYSbUIytVBGGUYI4xZdEzN5EztsLFmGI1oCZzHI+5wNPHEmbZ6gKYzeT7y74/+rxr2zlTQSe6TYZKIjDlAm1+eYYA6pDqS1aTF/P3H+FL7jESPTIljftkccZ7uBguiQaBlkskHPo0ijJUPt8lEawwhErMUkNiULs/xoA/C/Qmj57g8z2NVhfdJmUAw8Dyh+iTBvQpFbLkQKVCEbWFOfIiB3ccXMDSXIleFOyz6kkVQNlwrNcQ04g9yopBr8XNNuBlGLo0vD/swcnmkSmTVXO9nVbjtBkFH3Et8zbtN7lEU4QqvUUvdCYvvYG//aVZYWrcpvAx9R7dtyhQLBLPYBZnBvPVAHJ/zJ8ZGqyYdWgchl3PR5PeI6WESs7RbLPTGqjqFHog2GXyPdsbN1CplAn0RWLSAOX5FTJtEx4lyx3H1hQGZDM03ccJO53CtLJ5i5gzxICkMKLXuLmER/mDrhJWM5NlcNtoU+aMWtu/RJ94UrF0X7IneJYQKyuRBokVeDnGb0wz1iyTNvZv5rSAFjM8Y91Rn9zD5W51R8ItmLrNRMbUip5QKmISMYqhlncoVj0bnXGErWYfOccgeFsa3h69eWt7EweOnsSdpx+iB5DMkUjWVo9Sow2VXy2TF5XyWd0QoQ/xJO+kYlj+dUgSZQMFu/yR0AkKYhpl2KenNnYwYLhxH0hQGerb14142PmkaQq23M7t1G9MYyZsDHyItzPGbRUe7PMDzNaC+Bx3w6UfDMYJXRc8iwjActFCDEueo8x6uz2kESPM0WtEuXcYj136eNkT7KOrdwfEMGBvew9H7rgXj771PaivX0Wz1cAdj74FR07drfRgzAs0yW+m1Yc4McyM/yRr1epARkNNvEZxNldSr9pZv4HNrXVdzyiyaaCbGNVvnDEM+0OmMVNBmGAs0uIR3azMe792myidvGG2EKP3JVtYaiBhrbKHGUtcmxmMPiiA7ZpJDaneHamnVVIM6gzGKJKoL5InlTMusvyceWattaV5PPHUO3Ds8DGc/epfIsyWccc7/xEayKNebyGXzRGndtBp7FGeBAnmTJV+ikHY12IOZY1tOvDHfYLzGF62oBmuRV7Vo94L+L0j8sBOvYnurdeYGMa/xmgqz3qQrTshqT2Jv3/O56rGbGHKmJGps4ZKc4ZrRmqMgEbKunZiLAk1M0m/LeKNkDabolTKGCG9az7vSaRgm79bWyzi0dNrOLS2jMLcGgYkrLeun0M3t4Jr6x186aU/YtgCb/mRN8MrziFqXkebms7nhxoEYxhTjpjeSa5F4s9iuIoQ90VqEMc8epCE2phJQNYrEECahh69uXPrIkbtrWp+8fCHjDj8d6IeEgNpJtILzvLC/4VhTIrwZsIBZoyFmZBTAssFMTmhFycALR4ktRcFHH5Gh7sccYeGfLxATLjr0AEcO7hCdZ5Dm3gylLIoF+96eWQXjijjl284+uaf0fJo92t/geP8/YOPPIlTD9yLZ7p7CDdfBnM3SST5TYqB2C+aJeuTzJqGSIYGkrVJis/ly8iRjIp3DUYj2Fkaj6/tk/91N2+gv3kepcWjH2JG+yjhYaBEcR+A8T7+b36fCGKqjCfFo2npU6xrWFrOiJk6x0GsdWBZrG0kuqgrmYrPhWSpVQrfp+45jdffdze6JHl1ZjPLbiMfM+UKy6Wxti6cx+LqQay9kcvwiGrP/Q2WyjXcGHt45nId2ReexoHFOfTJi6J6VxmLhIsIzaRoluoxIymxxdwYwUCb3x1yTf54oGk+mydbL5YSIS5itlil8RpoM8w618+ieur18242+z6CxO/JBdvTSqxhfnDSpZiVE8lzxm3wE6cZT4pgI+7EmO4qfIauqSleQiwv4Sayn/zqibtO4cFTh/HsuQv4wguvohM7OHHfg1jjDq62e8qKg+vXsVCq0CNj+P0Gvvvsc/jDb3wPfm0Ndz7+Bnzr07+PD/zkW6nJlgj4HWLLjGdraEXpvpoql+SuTw+WxCRyYzTqkHj2lXhWqvPEs4yuv8T7ET2/0+2ic+M1etEFuKtnfp7Y+ntCWG0VfaZxiN/05Gy/a7bLMVsNMlK3NtRA5DwMKVmY8CzBBTFWxUvEYsgMcubQGo4fXMTfvnQOn332PMleFhUnxPLxUyhEQ+w99zUsHl4ibyKOEVvUAxiCuYVlPHnnIWTmljG3XMTIPUMNtQRQ5ObsiHzIJI6IJ85UKMRQZjzpIintkOTjMMyGw5ZqsUy+xFArah08CoYkkKQDlQW0O+tobm6ie/7bVBK51zMBHRqMB9fthFjhpyZxNjEObq8DzTQO97OYCFSDrmpZiWgdcEGCDTnTVs86MlfFnYeWuItk2WTTT51YxvLiIjLFAmqdW1jgLnZOHSMWEUCzFK5M6cPmHjIHj+HBp34Mx1fXSBK7MKgxTr7pJ+AtHsS5L10i1gWawiVNO3GyYdrwE/9JqYZpJGxaSKxLA4kn+cQ8gQQ7VQwBqYlQhjyN1KKqb5HEdjeuwQ7/isqp955+EHzcToif+U4jbQhGtzHneLbxNaPik5pzSK0lGSzLBYTKZA20RgbTZYAT5RzOHF7jlwsPMvGme09oCZUrRSzi0SVDjlQGw2batkTQ9nvorF9HhmKyUJ6D/dhbAQpMl6Hb740QUfxW1u7ia15GpRmh3u7TA5JlxjOCVSkLw2ySvaRwlhEtJul+PKIBpbOStHtEm4mxpC7W6XfQ6/T5+11sXD374+2R/3GxCSkdnpxtFN7OoM1puE2UitaxVUYktZ+MFt2g9L4fxFpQObW8jIVqmQuil3GnPWaxQnURCyfOoLxyBG6hQnfPKpEsiGC2XAwpmHt76wi7TWaqkGTTpm4qEGRpxXxFN8MqzKEkHpgzdTMTHTYjWo2E+ErjQIzT77bUcBk3R1wO1Yvkeiwrqd5IzSiUzMvneiSunU6dMqSIsV19crcbiFrCA/zQnAlzX2elraBpWM2GWVqFEYwUENT2i/4+0l2TT8wQFPNeNg3XSGCBnJ58qD+ii3OxVF55Q34yHAhWJjPbkJIioheGgyHCVkvb4H5rTwFGuVUyMsALt7WqKETTyzhJ/VmKP/tiPu292epBIlhV0ZM0mqnR9MP4OTHDkDCmm6zGokc3G02ucYhKbSEXW84DcrX3GyZuE2jGbSRxthmbdBZU2EppNSBD5q5LRrFSLjJf8DStbjQla4zUzLJb/mhMDUSMqe9RsY+44DwXnNFKQKtH4kb1v1gu0MAWBu0WDF78aOsKBuefTz550EHc7+vrZR1FsnLXNtPKg6EXHc/05CSshDCKQUZU8KbtaNkjJEkMta1lakFOsFRlCD9L1tnsMMxbdZTLVZRKxfslEd2jMGfupytjv82aiFj8QJlVxC2NYyHZCTN9r4hQm7/zRVvROM1uDzUpPRBD1O1lSKJLw0nHVT5m0EZ9ZwtOOMLqUhkNxn/UP08C5ypliJlCOpefQ47i0jt4iumYF0gKICFRKZUYtnX06JW5TApEKlgnXM0gkLu69CGNVORP181i0G0we4X6vOIm1ys0I0fX79FIg1GA3d1dlOfXUK3OnRGpcdzA7f9OhahhTDuk+6WPBJ9s1W6m1nLkd1JFzPGLTlJPDYU8UhS2aKQMQVHazEkxSnZ0qB7oxj56zQ0YNGqxXGLsD9BqdJGbK4B5GHZtDg7T74DA2b/wPAwyYKuyQmF5GeNeCxlvhWFso9UeqGxwp0X3eFJxl74YUzZJJ9cQhCMlpf0+wVuAW18aaaKRYBfyLZJmFFhoNBuq+vPF6jGpWx6aHVaYFahm6kkzAkONMsGdOJ7tLMS6yEO82FKtjLMkfmO6slQSB8STLEMxyoTaZon8EZ9rEUA7KJLzjEehyozTjx9D7fQDsGkIhyzaZUjMnXkCO8OBpnsyJfRaWxiT1JGro5gjihm9aT1ogkGTYBDMkbJHKPRDqIRXpLe46I7qmsGk66vlFHqs1q5pIZs41KVm7LZ2kV08clC6gvNmev3xpNW8XxFLW8iYAeu09h8njRVbypuM5QxlRYdZ7dtXb2KVF9BgLFe9jGaekFcfkXuMJaWGlrZ7hzRaZDjEozHMPBV9bZ47XoXdc5PJi51NkNphQNfPrh7ROpEfMiuRqWcYKv6gj1KumoR3Wu6IFaxjxRVZrKCSdDVEFgkWZT2GJbOZTy03olcJQreINwMaukLvKhfz8I0Q7dYA9foO1pYOLQgpL8xI94nsuG1myDCR1ozMtFoHJWHyj+ckvW0plAmt+cJrN/CprzyPG3tN9JhFJLUqqJNdxySMkrEjm+lWMIFs1mCYjPsMt8sN1L/zKhp/9SWEl64he3QF3336a3j6tz4DZ9hEZfUQP89Hp12HS8xRDkaD22m6nkj6eEJGhKeF0tWwtDsjGS0U0khDyDUMxUC8hNHIR3/Q1R58KeehlHV0TqDZbBHcu3npxxT20cfcH5JKDZWEVHwbg7bMlL1K5Y4LlBazrC/HxSyXsri2w2BgSImK71CVL0n72bITCSAG5oI9J8A3nr+IrXoH9x6oorIwB3++ivDEPegtrsLmLj9yz6OI3/h2MmmSNSr+XucSBs1tGEslZp6krRRPxv8m3hPH00HAIN0cAesgGpGWiBcViV+5ZOTFSHBqGCbqHuZYE4+Adq8/5PfVi2Ysg1TpRINp7Pe2E28xE+8xk+K83KQAPqlTSy3bMZPFCFhTn2KlkHCTq3UyUmaFFtN7n18mxE/TvUxykDhaGRuLBORDc3NwWxEWSHkOGSuomMvIbPlwXtlEcfEQ8iePwsmV4HoeXn3pO2jtbvFCI+U+5aylFQOZJ1ImPcWhZFwtTBm29OhVm1GsSmknXyzTSMLTIhTypBsMveYwxG6zi+5goGUeSXTddlNt0sG+Y04nucSLzLQHOxmiMmcHO+OkjuQ6pmYAEaZSgJKQWyq5uFHvYsQVSpZq0EDD/iDxQinqExh9ptO7jq3gsfuOYvHIKgIx3uYurEuXkGXGGdcy6C9mEJEzCQuukzR+/7tPo1bIM3P1tVcvxfcsmXiSLBLCKDikTFlSTGzoYwkxm3REMqgU8KUmJA1E2VTJxiJeR2aO4B3rsISoAmnBd3uDrux/15gUzaZ96f10b07kxoQX6ewPUrlBsiY6Ji2BCNmS1dYIunL3VmOAMonjHnel0ekoo7UcS0F7RCDv11uagnMLWdh3VdE9lkG92ENjvEGqP2RGYegyPJdWV/DF//3n2LtxAWurS9jabfNifeJHQOww1UiKi1E8Ta2mVtvJzP2kn29rNqM0YgaVkT/BMT9MwtCTMTJHUr2DjGy6Fau4HQyDrtCBZjyRGTPDmfuTREaaFMy0LJvcF0NF6Q6Ypq0sR3pL0p2QAtZyMYM9pktZTJ4G26FBWjL+pvOARjqkRR0UywwAeQwfF7JF5JbWVKw6Tp7GYegtLODilVfxP37nt3FqVXhRH9t7HQpkSyfQpInouY726uN4JtVLMY2fKXNO2mGxM9omkpqQoPNEi0kFVISytJ1k0kRAXQzk8NrHYbwjNPmWiej20d9puWNmVnGS0ZRIm+pxQtDkCddJ9JGAm6R7l/I3T0P5TOVNMt3lhQqyRRe79KIW5YaUIOS9XIBmGgX7fBUOhajrlclH8gxNT3v1HrPfRz7yYYzIbu+78xCN00afuk5kRsGVqiGzmR+qUE6MH0/xMSCQCNeSWUVJJvKagFJDvt9MIyGigU2uweBzvlYhLdWPMhVHq9yQNH8pnhmUxA9psnQKdoJF8g4pt9JrRKwKEBbp4qG2gykiM0kLO0fEtgTgen1V1gtzJJBlMlkSvsFQBihDrUcHIS/EN3Rh0TjUkBtTgUdksgcPruKTn/k0vv7Xf403PXynLnpjt58OSEk3l3jBVY5GUlWIdYTGSJsQlmZXMEH0MaSOkxB0bFsHzGNmSMtIivsK8DSODDDIhvmxNUOAjctUO/j+bM1nOjeR2GVfm03GJ6a/M1UT+fzQMpWEXOyAKyrnMyg4Xa31DPm6FgmjeFKpUmbGyCGUcgMX42UMGlEgQxZMYdpq8CJc/fgo6OHAUgV//ud/jI9+7ON44sRBLNWknkMexGyTcy0dfM8wC2V4v93sw4uSgSmtK4hXqsFs7fP3ez1Uqr62qQQH/XiQ1M/t5PXCjySKhK6QymqysfRYgPGyFCy/N50DMPbrPebs6Pa0kmhOgVtcWWopvrZPMtrOkbGXNXqKCj96gzw34IL6BOl5GUpT/kNmG411qsNzhRPJThLBCMqWtcOrG2Hx4FF895Vn8A9/9cO4kyL21MEFTQD11kjLrG6WGYngnWO28wisOhNJTxAJI8W6Pjdl0OsqRsk/fYb2uN8m6EfcSJ9GCPjasRpIB7IgLSvwJoUpl98lxpERwsz3ZND2BT7dl6nV6aBCOmaLmYHqiQeZ08acofgxkp2i63o0SrtHvePMoZR3sE2AFgowZBgOmOKFyVpClGhMR37CY5r11U0d8pkMPc8my/XmVjF35wP44je+AtcYYm3xuCp7h+x2Y2+ooauz1FLLYViLF4VxUj0MWi16K3mX1LZHLZQzaeej38SwnuHGkaONejQuwXrQTgStDDvQiwtc0oC42ScOmcS/nB30/dh5QcSqzxV8k1f+9tkBhdvq0ebMeQ3RPRq/ySRFoyd97zksMc5uNXrKnJerRVzZbGr67IskiIkDXbJYUnkzJ33zDIlillnG1x6XkcnTXmTH1FZuZQ3teh0Oddc7Hz8CsaESziGFKvVJNecoZggPC4OYGyLVQkfnEAc0zrjHCyfGlXlZdt5RbuQHXRqIGyn12WCkAxXjYV8NHEkHM5AKA4kujd2RsZmIySaKv8ks69upCb5Ac7x9Urg0ZyYQZx1pFqWSYwQmbjGrxOYajizXlBxeXt/DPUcXUSH/Mfpj1DtcRMZHdTVG0BmQxeaV3rv5En9mERFATbJkt1RTOWF6BeIU1buM8JEqzBWycMYOLm23UGMciOIecFNkpltIn01D2fQu6c2ZUdKr15BR/Zi0xAMy7x7DDPwueWYQWUoIhRaIsXoDakZjpI2FcZhBm14UDEZ/NRYKEyce82c6Bzc9NjDTPIyN20bwNZsREAPuvHhShyl3kyC5VC0wtDLYJcstZkwcXK7qKSLJapcvtlBYIsO+f0HqtBr7MmmRKS3BWzhEKVFR49iZXDLQy5DKU8g22kMKSAPVooOttgxbycS+oy6tmk5mAaQIwos06WU2yZ8MKPADMQykeUn8Gw61w9Il3Wh0h8yqgomBhqVEhEyldEYh6UigE2+BlGkpATqj6PMdnYBLrv06yd/T0zH128qs8f5s8rRiDc0iMqDJpeH5y1vaMLxrrcYs5uLaRgMnl8vae8p7xKbWGJeevYrq8TLmHjhJ3pGUPC1eULZEAC7UCNJZvXApiWp9m7jW7o5IRF0Uy656gyscS8oXUpFMD8BItnGkoiC1H48eRu5kehVJAejTc/pk3MK5BNy5l/SYSCsMAuxSavVpSIINf2/odIlkWKb+p0mBryfRFE+PHPxXA7MH5m53oOl5GyPWmu5IdE1saBa4stPGZqODOcZ8hru82eyhTLJ4co3ygbtTLGSw/lod69/8DrLH6GkPPKw7b1JdOxpuZQJ4luBIAWk602MMY+FHho1SzcbBA55uiikFWzMBXyn3RuM+8lEHJc/WBqEhrJreKUQzHWLWBqc9lUdcMz00DKJ0gk+anZFSANGRriHM2v9vli3ZzJU+2zS1f5Z3d3F7RP3wjBBvY9J3KVuKoJOsJ7T//7x6i+Bm4MB8SQc4N3dbuHOlrK2ZgQABPaJJhd544cuIF2KUzjysi5VzXQ6VtfamDCvxIqbjAsO0T7ffbDMzMnSPHykqQ7eQVDMFuDMZF13u/mu3thF1mygxa1bznjJ7PTdmGXpz9WecVBjl+JXQCpkr0Bp24lGiDqT4x7Xv0kD/M9LvsKaTtLJjAz73X7B/yPAHQmvfjyLij6T05kCmxWiUkoM2ge75KzuolXJcoIVLDLMMd+reI/PUYaQDMj/EMMpu8X2vfA5DZwu50w/Sc7jrQZCw2mEHMT1Tyq9lZispmG8Q08R4ywtFFEuWhoXNi3EZTtLQ6wYGvl8P8O1Xr+DmjZvY6wx1RjLr2pNicFKZSPtCURI29KhE9WtHNgwVkyLleeYnjNgf+Doz7sCcNpSSSS05GdiYmfefMVFyAE4WJYOWYhCf7u9xJ5eYse6ht6xvN/DSpS2dI3S5zRdu7OH0cgFrJHvr9TFGzDpLc0dQDvPYOfdZfsY5ZO+4G05pTkeQhbxJApDvy+XyWKnm0JMjDES65cUq07aMs9goFj2d0pDB00gmWOkVNzs+zl67he9c3MBLW33tdWmVc3rQJda+mx+lE/Rpz04LkWGo43hBbDVopI/JhJxMgkjRL8GgtBrHj2nRgf/9pGgfT88vJECdo+ZqkYxtbG4Tb7I6e3O2McbFOneNsX/fwZrO/K3Xu9pSkfRZ32vh7uUcFa1kpZC6qIeiswLPKKK1/hw63WtwD6zCWzzAr7G0YShh7xbKqFCLyMheaSGDylwBeRosIkaUSBUypAb5ShV3nVxTSiEZsW3kyJhbeI3rOdtEMvvOtcsghfAh6bxIczCIf+AIRSwgTgFtWP+Bur6lNhFBLX3+Bx9/RMYgyDvGWjuJw+g7tPZ7GK9LpmlOR4AL5CDNVgdfe+kCyWAfB0gMbRK+vVGEazSITLCuUrGvVHK6S02GoIhVUcsS8/OVvBbeq6SsNhmrVV6CUTsqbU2+ZoTCyjEdGB9ub3C/ZOi7hcb6Bdz38DwWj64hqqwiJI8Z7bZRrS2qAQVIlyoOrjFLXt4J6FVFvqZFLsgMy0xWJBPPxdwsM6lyBqLWU2xSIRsm7aos8clzrZdNy/3HthlHrhUmDJzM3nrgsUeIAaEaRzhAzCviZ32fL/iAmQ5M5wiGApxffuECmfNA+0siDJdpkFIuS3yxsUd+cWWvj8W8hZNLJY3LbRLDLPFIHFSmweYqnp4TyxVchgYXMX9UW4jCjMX9s3NLcAoFGpZkbauFvH0VB+49gNLJd+HQmffTI3fQePnvsLR6B6xsjiBNZtzZ5WYHeG6dnEXErtSe6aVjfoaMyRzIEWvkOBbXIaAr95MqqKFTtiK257J2XM1l3ms6zlXRkSVqPXl9yPC17qeBpB4iZ7bkIJ10K4zYuM4PWCRwvk4O4BaYQr/96lVc3W6jUiwwe7nMTCRZ0mOnfMjQkwTy2uOYj6UE6utZ1KVSIj/0yDc/u8psNE8xm+HFufxdUFzmjkYqWxQryIHKq3ei1X+GGewlLJx+Aw7d/9NYbzSxV2/gxL1PoHn2aXKtip7AzzPTnb14Hdcv3sTILWC9F2spI6KckPVYTh6Hi8x2VqT9MVXveio64VAtUhDaCIsF91Mlz/lUJAV+mStwHS0EjgxrKtr3+2H7Cf5X+b+zBTLhl69s4NUbu7zAnH64TL9HVhYbzHtX2gwRfnlZhr5rZeyO+OW5Iq73xnr8cW2hgo3uGFs9HzfJl2RaNaanUtfr+VQRr3JUSXSZnSli+/JfoP7aJ1CoMW0fP4PG1iv4zf/4L/Hzb3gKz33zq7j3HT9Jj2tj3N3DqLmBbXKuK9fWcaYwwrwz0JqO3KS+00UGrcDUUwDxZNxZB94FhJNLJck8G5vmr2oBj6xapnCF9EqxzeCLrAcefSQZ5ZWBS+ljyS9Ez5Bklgvel2/VW+//+osXPVfPacXajxLCJp4lRyWlWBUKmSsWdf652e7gzoOLGuvfPb/B53NM2ZQgTL+dvq/4sFRzsLA6B3vtXqalPIruGLVqDX5vE93et5CrHEQlfwQVUob/9ce/hede2NGhgvWd6zh0oIKtcxfVADdv7mJjp486N+BIgQbIlXG1FeipI6EioVNA2fZRM7p6TEJJITFP8FIY9iCMW5mM87ashU0p1MlAFjOZQs1YHjNKiEGv0xDTcwxiIP3rC9A6L2N27+svvvZss9P92ayQG60oWkqsRA0L45VTzCOSRqkICsgKy+70h9gdyryN/FUHX7mKcI0Lu2S9NOLdRxdQJXeyM8wqXoSN9Us4e6mLyONXnLsGZB/C1biMC+evYS84hKE5h8eWDlA+UG4MbmD7VheXrjfQJcYJfxJsiUg4pW50q03N1fcV07TJSYZeRV+HOKV+LUYQaUGiQ9URv5vX9V3Pkn49VT7jTZi7JBZpLI4NIwVpXpScrZLTNZEcI6ABaqUCXrm+gXPX1q8UPPc8qfhPUx+ZIi2kpySxLDM9hWJJR9p67SZsL0e1zvRNIG+1u1LjxJIHvLbVIeEb4Fg1i6OLBZw+tqItmYo9xp989Xv4z599ibtZQlNOIH/jArwXX8S3anfjtW0DS5euYW5xEbkqPWp+HvfcV8Hexk187dlNSpgsasQ1rYdzTaeoBdtU5Gd3Az1irvXnbA0FSSN+Rz1CSrMBuYIfG++nxPhLMW7BTQS4tMOhtWtbu7ZqoPsfFQ+KtOwY8MNDqt8cX1QnQXvm/BWNW9MyX4njaIMf8y7Ltg1JgZb+cRFDz2TlihXNgvIHUnLFqgJut7mHE6skd0LzCcQOPfIdZ5ZwB7lSJSe1am5Kv4+FlaO4Yt+PSplZabiF8hPvRj1bIa7ZzEAOhgzZ3IFDuO6QbefzePihB+D4t3Dz/E1c3hpo2Mvpn6qeB4kYgnN4eYMUYWRr9h0ykRSsAJW4p0U2HYA1zH9KA/3+MEy6q4Kzct5Eqg+xgrirWjGWasF9jzwMU48TBEyPI53w8vgpL9/cxEarrdwlacjFz9MwlxlWP8H7pri2SXD2/ZEOTmZLFbQaWwri0tpt1He0PCEjdI4Z4rE7D2JlqYKvXh7gcNnBctnGjY02auEeHn3nO7QDO2i28I0GZcnCMmhtbL74DJaX78J1KcxbDn7kvuM4tHoXnGIW4cYzuHZuneI0z6zmEdeYGZnVZHMFJ589f0u/W0LMjcdYIYAbphFYpvHB2DB+exybOi4ohDZHA8lhY2HzQiJNS7JYMkajHR+l4nF6MIS7IEVuqaVknCQe066QFOn/gPjzToZgO2mNGPCYsgdyxEhq0+QNjd1NLS9I2EmtxWZGa8UemiSU8wsLzG4G/tPX6rjJ3Z8n2Xzt3E3Y3/8a3vf212GJ/GnnxhX06009a79TnMeBQ8cwvHwNr3YHsAsVeGEf2aXH8eQv/Bv8zHsfxIrZw4DhLJhz6NgaLyLA3fNZ3L1IcbyznsAHMyW3lGsO38Wr/T1TSYAqLy20ydEtyXJSHxd48cVhEl3GEKMHWQLSkZzQGyLsdLW4vUfAbvKxNTM3lJ6EucSP+bzjum+mZReNpHmfTDVSDYuBisWyLmBnZ1sHME2vzOe3sN7y0QksXG1Tge/4WCMGzdWKeP7r38TqgQP4kff+M5wskyQSSBeWDqLcr+Pa1jUsnzoNY7yLR07cyYyYRbfbQvnw63Hg9B3ItZ6H0+/oYHhhfoHA7zC+GziysoRXd7rYaXa5iflXFt3obVR2zwzDhFFLNhuE6cFfYphUJAwaeTSmQSUULZJZgZJ7X/cQQyzSLONz13xqrSLddMTbdbq8Y6Z/usZIZIdwiCDw9xw387tOtlgLxsOHqbYNHUWh3up1GtoLly/Y3NogxwnIaVbpMXVmKnpLvooqmWo9dHCOsvhEAahQejz35S9i7dQpPP7Ye3Dzwt/g0qVXYSydwlFi3MbuOu674yTe/OBDevY1pjQJmUXjzBxKixbm5sl+SaINElUjU+J+RahYY9KQfHyx5f/mtVsb7x3B2cznS9zTQDPtkJlVpjoEtGWPJcyk+DbWXp+p649leve+h2kgEXK05FhO47SapFfABrPZNrmHHorTv/UTT+eGpH9E3uAXSrUv2F7m73j/CVpxTorxMpg07LRQKFW17St8pFSuory4itbOJkoUltJ4lMJUx8ziuY0RFmiw+bCDKy99C0uPvhn3nL4PN17+HM5dvYllSoZqpYA3/diPw6HrS9jGUdLjkvpRQMopA59OuQQ3oNGYZFxioOUaF2rx8H1+YH3iZrPr77VbWI8qqFHmFOK+HoOSkz6CQXZqIJEXOhyvY3y21s7pQQ8yxILkECwNFDToNbTITbLJOkPMmZydj+P0PGtipDF5R0RjFCvzkuo+TT3UIgd4hJnE6zR3kS9XFL9Gva52ExaW1hSbdjfXsbiyqr0yYiqaoYXnGW7V2hxO+jdJrMdYeuRnceaeRxDuvYLvNXbw1I/9FE6vHKQO7KlRxAviSPpaWeJGjtdSRJ87PmCmzBj9pudZv8b9/EB7r3UxFxCcCdaXtzt68afmHFTcBGMGxB/xIjowSuRgMvQunRjJuprNuG7rzEP368ibeNBIPKjZZMjFuDEc6ckcx0yb/HFSlrTSbqSIvaFMZsmftqjOk0b53wpGo99ipiBm904To/LiUf1OO+liyPkLGq1BtS6l0drcHBp7uxSGGT0p/ULT0umKdz56GAfOPEaZUsLx4w/g2LKDtaUjcmCAQOyn5QlDBa4kBcvKwM6SlWcXt83qkY9a8yt/H1n7K/TdsHXrgrRvGIpFXG0QX3nRdy/Sg+xIcbNLYig3KbBVcsxkNJA0KGUwXg/sqAc9dJ9aVlojPrNBTAOFfHyVnGgUSMPOTP+aSzIvJGMk2jhMBwXkTKhHde9m81KKHZIzfZ1y4hORP76UL5cX+u3OQYlM+ZMTwpFk/KSxu41VZqeQWCJHweX8qTQeX2q7uLgzxv0P3IXDi8f1+/OFA2g2L+tAp2HldFghmRHQudaQm/aVIAr+tWl7/6RUWP7bXOnw0PfmEM8fRvboSbTOv4iN+ghXjUX9WwDH8iPtuArOtIln7dRARQlTuVnJH2KRPDeWgpmkxyTNJ7M4IjtUr0TR/p9dSk/PSFtXR14EhCVGbVf78zsb1+WXyJMLCV5ZjjUcDYa/G8fGk8wgR2m4Xx4O+l8KyPezpZqSy53tbVTmFrWu3Go15G9k4AjT/F9861U8/pa/h4/+xkd0PEbGVmx3AXuNK6QbXbL9vUEw3vqSaUW/HMI8xAdvG4fBH8l4fDDuau89l13B8vw9eOChD+B7pZP4zFefp9xxUCuXtbw7lDKHn/ylOzMdUJb+mB5MjqAdELGFSJNkgE6MlA4rWbzfkg/RXybK3koLZ+Ix0g/TuT+GiTBO+SnCrr6zoa+VI4868ibTrdRhjuddC/zwY8N+50fHg17ZyxUetWzng429rQ8zo/5ZbXH5+8yMm6NBr98m/r31TfdTjff7v/PfP79puhZ/N/6zQm75w5a79MG9xrVHgSGv0vlR0pKPxUa4LhnJNo00w0aKT0bs61Gn2B/gPR/4V8jdfS+uXjxL+jLU3vxg6KvW8tP2k5zUkL9UMxwk3Vk55y9/rkdOB/xfAQYA1ZlE5oZXGP0AAAAASUVORK5CYII=" class="img-responsive center-block">
                        <div class="spacer10"></div>
                        <p class="toupper font14px fontw800">john doe</p>
                      </div>

                      <div class="item">
                        <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin feugiat nisl maximus enim malesuada, vel viverra quam rhoncus. Ut sed tincidunt dui, a varius mauris."</p>
                        <div class="spacer10"></div>
                        <p class="toupper font14px fontw800">jane doe</p>
                      </div>

                      <div class="item">
                        <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin feugiat nisl maximus enim malesuada, vel viverra quam rhoncus. Ut sed tincidunt dui, a varius mauris."</p>
                        <div class="spacer10"></div>
                        <p class="toupper font14px fontw800">john smith</p>
                      </div>
                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                      <span class="glyphicon glyphicon-chevron-left"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" data-slide="next">
                      <span class="glyphicon glyphicon-chevron-right"></span>
                      <span class="sr-only">Next</span>
                    </a>
                  </div>


                </div>
            </div> 

            <div class="spacer50"></div>
            <p style="text-align:center; color: #fff">Copyright @ 2016 Emarky B.V.  - All Rights Reserved</p>     
            <!-- <div class="spacer50"></div> -->
        </div>

      </div>

    </div>
    <!-- .container -->

  <!-- Modal -->
  <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="border-bottom: none !important;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center" style="font-size: 36px; font-weight: bold">Next Class Starts In <span id="nextmins">{x}</span> Minutes</h4>
        </div>
        <div class="modal-body" >

          <div class="row">
              <div class="col-md-12 text-center">
                <div class="clearfix "></div>
                <a href="#" class="btn btn-extra-lg btn-orange btn-noborder" id="btnregister-3" style="width: 100%;">
                    <strong >REGISTER MY SEAT NOW!</strong><br>Very Limited Seating... Register Now!</a>
                </a>
                <div class="spacer30"></div>
                <p class="font20px">You'll get our upcoming unreleased software <strong class="green_text">FOR FREE</strong> (no catch) just for showing up!</p>
              </div>    
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="modal-footer" style="border-top: none !important;">

          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
        </div>
      </div>

    </div>
  </div>


    <!-- Modal -->
  <div id="myOptinModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="border-bottom: none !important;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center"></h4>
        </div>
        <div class="modal-body" >
          <div class="progress">
            <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar"
            aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width:75%">
              75% Complete
            </div>
          </div>


          <div class="row">
                <div class="col-md-12 text-center">
                  <h3>Select a date and time for your webinar</h3>
                </div>
                <div class="col-md-12">
                  <!-- FORM START -->
                  <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" id="frmWebinar">
                    <div class="form-group" id="divDTime">
                      <!-- <label>Select a date and time for your webinar</label> -->
                      <select id="webinar_dt" class="form-control" name="webinar_dt">
                        <option value="-">Available Dates</option>
                      <?php

                        ?>
                      </select>
                        
                    </div>

                    <div class="form-group hideform">
                      <!-- <label for="inpName">Name</label> -->
                      <input type="text" class="form-control" id="inpName" name="inpName"  placeholder="Enter Name">
                    </div>
                    <div class="form-group hideform">
                      <!-- <label for="inpEmail">Email address</label> -->
                      <input type="email" class="form-control" id="inpEmail" name="inpEmail"  aria-describedby="emailHelp" placeholder="Enter email">
                    </div>
                    <button type="submit" class="btn btn-primary btn-block font22px btn-round hideform">Yes! Reserve my seat!</button>
                  </form>
                  <!-- FORM END -->
                </div>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="modal-footer" style="border-top: none !important;">

          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
        </div>
      </div>

    </div>
  </div>

    <div id="getting-started" style="display: none;"></div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap/js/bootstrap.min.js"></script>
    <script src="js/jquery.countdown/jquery.countdown.js"></script>
    <style type="text/css">
    .hideform { display: none; }
    .lds-spinner {
  color: official;
  display: inline-block;
  position: relative;
  width: 64px;
  height: 64px;
  margin: 0px 45%;
}
.lds-spinner div {
  transform-origin: 32px 32px;
  animation: lds-spinner 1.2s linear infinite;
}
.lds-spinner div:after {
  content: " ";
  display: block;
  position: absolute;
  top: 3px;
  left: 29px;
  width: 5px;
  height: 14px;
  border-radius: 20%;
  background: #0EBEB4;
}
.lds-spinner div:nth-child(1) {
  transform: rotate(0deg);
  animation-delay: -1.1s;
}
.lds-spinner div:nth-child(2) {
  transform: rotate(30deg);
  animation-delay: -1s;
}
.lds-spinner div:nth-child(3) {
  transform: rotate(60deg);
  animation-delay: -0.9s;
}
.lds-spinner div:nth-child(4) {
  transform: rotate(90deg);
  animation-delay: -0.8s;
}
.lds-spinner div:nth-child(5) {
  transform: rotate(120deg);
  animation-delay: -0.7s;
}
.lds-spinner div:nth-child(6) {
  transform: rotate(150deg);
  animation-delay: -0.6s;
}
.lds-spinner div:nth-child(7) {
  transform: rotate(180deg);
  animation-delay: -0.5s;
}
.lds-spinner div:nth-child(8) {
  transform: rotate(210deg);
  animation-delay: -0.4s;
}
.lds-spinner div:nth-child(9) {
  transform: rotate(240deg);
  animation-delay: -0.3s;
}
.lds-spinner div:nth-child(10) {
  transform: rotate(270deg);
  animation-delay: -0.2s;
}
.lds-spinner div:nth-child(11) {
  transform: rotate(300deg);
  animation-delay: -0.1s;
}
.lds-spinner div:nth-child(12) {
  transform: rotate(330deg);
  animation-delay: 0s;
}
@keyframes lds-spinner {
  0% {
    opacity: 1;
  }
  100% {
    opacity: 0;
  }
}

.form-control {
  height: 42px !important;
  font-size: 18px !important;
}
.btn-round {border-radius: 8px;}
.progress { height: 30px !important; }
.progress-bar { font-size: 18px !important;padding-top: 5px; }
    </style>
    <script type="text/javascript">

      // console.log(moment().format('YYYY/MM/DD') + ' ' + moment().format('h') + ':' + moment().format('mm')  + ':' + moment().format('ss'));
      // console.log(moment().add(1, 'hour').format('YYYY/MM/DD hh:mm:ss'));
      var mdate = ''+ moment().add(1, 'hour').startOf('hour').format('YYYY/MM/DD H:00:00');
      // console.log(mdate);

      // var roundUp = moment().minute() || moment().second() || moment().millisecond() ? moment().add(1, 'hour').startOf('hour').format('YYYY/MM/DD H:00:00') : moment().startOf('hour').format('YYYY/MM/DD H:00:00');
      // console.log(roundUp.toString());  // outputs Tue Feb 17 2017 13:00:00 GMT+0000

       $(document).ready(function(){
          // setTimeout(function(){
          $("#getting-started")
          .countdown( mdate, function(event) {
              console.log(event.strftime('%M'));
              // $('.days').text(event.strftime('%D'));
              // $('.hours').text(event.strftime('%H'));
              // $('.minutes').text(event.strftime('%M'));
              // $('.seconds').text(event.strftime('%S'));
              $('#hours span').text(event.strftime('%H'));
              $('#minutes span').text(event.strftime('%M'));
              $('#seconds span').text(event.strftime('%S'));

              $('#nextmins').text(Number(event.strftime('%M')));
              // var xmin = Number(event.strftime('%M'));
              // if(xmin == 0){
              //   $('#nextmins').text(event.strftime('%S') + 'Seconds');  
              // }

              // if(xmin == 1){
              //   $('#nextmins').text(xmin + 'Minute');  
              // }

              
          });           
          // }, 500);
       });

    </script>
    <script>
    $(document).ready(function(){
      // $('.btnform').click(function(){
      //   $('#myModal').modal('show');
      // });

      // $('form[name=webinar_form3e749c48af] > input[type="submit"]').click(function(){
      //  alert('asdas');
      // });
        $('.btn-claimspot').click(function(){
          $('#myOptinModal').modal('show');
        });

    });

    var is_going_out = false;
    $(document).on('mouseleave', leaveFromTop);

    function leaveFromTop(e){
      console.log(e.clientY);
      if( e.clientY <= 50 || e.clientY <= 0  ){ // less than 60px is close enough to the top
          //alert('y u leave from the top?');
        if(is_going_out == false){
          // $('#btnmyreg1').trigger('click');
          $('#myModal').modal('show');
        }
        
        is_going_out = true;
      }
    } 
    </script>

    <script>
    function ordinal_suffix_of(i) {
        var j = i % 10,
            k = i % 100;
        if (j == 1 && k != 11) {
            return i + "st";
        }
        if (j == 2 && k != 12) {
            return i + "nd";
        }
        if (j == 3 && k != 13) {
            return i + "rd";
        }
        return i + "th";
    }
    var dnowDisp = 'calendar_text';
    var monthWord = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var dayWord = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var now = new Date();
    var newDnowDisp = dayWord[now.getDay()] + ', ' + monthWord[now.getMonth()] + ' ' + ordinal_suffix_of(now.getDate());
    $('#' + dnowDisp + ' h3 ').html(newDnowDisp);
    </script>

    <script>
    // var timerID = 'tmp_countdown_evergreen-47602';
    // var now = new Date();
    // var dminutes = 59 - now.getMinutes();
    // var dsecs = 59 - now.getSeconds();
    // $('#' + timerID + ' div').attr('data-minutes', dminutes); 
    // $('#' + timerID + ' div').attr('data-seconds', dsecs);
      
    // var xmins = 'tmp_headline1-85415';
    // var xm = $('#'+ xmins + ' > div > b').html();
    // $('#'+ xmins + ' > div > b').html( xm.replace("{x}", '<span id="sp-'+ xmins +'">' + Number(dminutes) + '</span>') );

    // setInterval(function(){
    //   console.log('val => ' + $('#' + timerID + ' > div > span > span:nth-child(2) > span:nth-child(1)').html());
    //   $('#sp-' + xmins ).html($('#' + timerID + ' > div > span > span:nth-child(2) > span:nth-child(1)').html());
    // }, 500);
      
    var dnowDisp = 'calendar_text';
    var mom = moment().tz("America/Los_Angeles").add(1, 'hour').startOf('hour').format('Ha');
    var mom1 = moment().tz("America/New_York").add(1, 'hour').startOf('hour').format('Ha');
    $('#' + dnowDisp + ' span ').html(mom + ' Pacific Time , ' + mom1 + ' Eastern Time');
    </script>

    <!-- ADDED FOR WEBINAR REGISTRATION -->
    <!-- <div id="tz_info"></div> -->
    <script src="timezone-detect.js"></script>
    <script type="text/javascript">
    function getSchedule(){
      var GMTDate = new Date();
      var d = GMTDate.toString().split(' ');
      $.ajax({
        type : 'post',
        url : 'get-schedule.php',
        data : {
          'gmt_offset' : d[5]
        },
        success : function(res){
          $('#webinar_dt').append(res);
          $('#tz_info').append('<br/>' + d[5]);
          var tz_ = show_timezone_info();
          var ctr = 0;
          $('#webinar_dt option').each(function(){
            if(ctr > 0){
              $(this).append(' (' + tz_ + ')');  
            }
            ctr++;
          });
        }
      })
    }
    $(document).ready(function(){
      moment.fn.roundNext15Min = function () {
    var intervals = Math.floor(this.minutes() / 15);
    if(this.minutes() % 15 != 0)
        intervals++;
        if(intervals == 4) {
            this.add('hours', 1);
            intervals = 0;
        }
        this.minutes(intervals * 15);
        this.seconds(0);
        return this;
    }


    var x = moment().roundNext15Min().format('MMMM Do YYYY, h:mm:ss a');
    var y = moment().format('MMMM Do YYYY, h:mm:ss a');

    console.log(x);
    // $('#divDTime').html(x + '<br/>');
    // $('#divDTime').append(y);

      var d = new Date();
      // var offset = -d.getTimezoneOffset();


      // $('#tz_info').append('<br/>offset : ' + d.getTimezoneOffset());

      getSchedule();
      // $('#tz_info').append(GMTDate);
    });

    var loader_ = '<div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>';

    $('#webinar_dt').change(function(e){
      e.stopPropagation();
      console.log($(this).val());
      var val_ = $(this).val();
      $('#frmWebinar').append(loader_);
      setTimeout(function(){
        $('#frmWebinar .lds-spinner').remove();
        if(val_ !== '-'){
          $('.hideform').show();
        }else{
          $('.hideform').hide();
        }
        $('#webinar_dt option[value="-"]').remove();

      }, 5000);

    });

    </script>

  </body>
</html> 