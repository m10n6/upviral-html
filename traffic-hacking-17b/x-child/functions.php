<?php

// =============================================================================
// FUNCTIONS.PHP
// -----------------------------------------------------------------------------
// Overwrite or add your own custom functions to X in this file.
// =============================================================================

// =============================================================================
// TABLE OF CONTENTS
// -----------------------------------------------------------------------------
//   01. Enqueue Parent Stylesheet
//   02. Additional Functions
// =============================================================================

// Enqueue Parent Stylesheet
// =============================================================================

add_filter( 'x_enqueue_parent_stylesheet', '__return_true' );



// Additional Functions
// =============================================================================

// Output Primary Navigation
// =============================================================================

// if ( ! function_exists( 'x_output_primary_navigation' ) ) :
//   function x_output_primary_navigation() {

//     if( is_front_page() ) {
//          $menu = 'Top Menu - Main';
//     } else {
//          $menu = 'Top Menu';
//     }
//     if ( x_is_one_page_navigation() ) {

//       wp_nav_menu( array(
//       	'menu'           => $menu,
//         // 'menu'           => x_get_one_page_navigation_menu(),
//         'theme_location' => 'primary',
//         'container'      => false,
//         'menu_class'     => 'x-nav x-nav-scrollspy',
//         'link_before'    => '<span>',
//         'link_after'     => '</span>'
//       ) );

//     } elseif ( has_nav_menu( 'primary' ) ) {

//       wp_nav_menu( array(
//       	'menu'           => $menu,
//         'theme_location' => 'primary',
//         'container'      => false,
//         'menu_class'     => 'x-nav',
//         'link_before'    => '<span>',
//         'link_after'     => '</span>'
//       ) );

//     } else {

//       echo '<ul class="x-nav"><li><a href="' . home_url( '/' ) . 'wp-admin/nav-menus.php">Assign a Menu</a></li></ul>';

//     }

//   }
// endif;


// Excerpt More String
// =============================================================================
  function custom_wp_trim_excerpt($text) {
    $raw_excerpt = $text;
    if ( '' == $text ) {
        //Retrieve the post content. 
        $text = get_the_content('');
        $text = strip_shortcodes( $text );
        $text = apply_filters('the_content', $text);
        $text = str_replace(']]>', ']]>', $text);

        // the code below sets the excerpt length to 55 words. You can adjust this number for your own blog.
        // $excerpt_length = apply_filters('excerpt_length', 55);
        $excerpt_length = apply_filters('excerpt_length', 70);

        // the code below sets what appears at the end of the excerpt, in this case ...
        $excerpt_more = apply_filters('excerpt_more', ' ' . '...');

        $words = preg_split("/[\n\r\t ]+/", $text, $excerpt_length + 1, PREG_SPLIT_NO_EMPTY);
        if ( count($words) > $excerpt_length ) {
            array_pop($words);
            $text = implode(' ', $words);
            $text = force_balance_tags( $text );
            $text = $text . $excerpt_more;
        } else {
            $text = implode(' ', $words);
        }

    }
    return apply_filters('wp_trim_excerpt', $text, $raw_excerpt);
  }
  remove_filter('get_the_excerpt', 'wp_trim_excerpt');
  add_filter('get_the_excerpt', 'custom_wp_trim_excerpt');

if ( ! function_exists( 'x_excerpt_string' ) ) :


  function x_excerpt_string( $more ) {
    
    $stack = x_get_stack();

    if ( $stack == 'integrity' ) {
      return ' ... <div><a href="' . get_permalink() . '" class="more-link">' . __( 'Read More', '__x__' ) . '</a></div>';
    } else if ( $stack == 'renew' ) {
      return ' ...<br><div style="float: left; margin-top: 15px; margin-left: 5px; margin-bottom: 20px;"><a href="' . get_permalink() . '" class="more-link">' . __( 'Read More', '__x__' ) . '</a></div>';
    } else if ( $stack == 'icon' ) {
      return ' ...';
    } else if ( $stack == 'ethos' ) {
      return ' ...';
    }

  }
  add_filter( 'excerpt_more', 'x_excerpt_string' );



endif;



// Content More String
// =============================================================================

if ( ! function_exists( 'x_content_string' ) ) :
  function x_content_string( $more ) {

    return '<a href="' . get_permalink() . '" class="more-link">' . __( 'Read More', '__x__' ) . '</a>';

  }
  add_filter( 'the_content_more_link', 'x_content_string' );
endif;

// POST / BLOG META
// =============================================================================

if ( ! function_exists( 'x_renew_entry_meta' ) ) :
  function x_renew_entry_meta() {

    //
    // Author.
    //

    $author = sprintf( '<span>%s</span>',
      get_the_author()
    );


    //
    // Date.
    //

    $date = sprintf( '<span><time class="entry-date" datetime="%1$s">%2$s</time></span>',
      esc_attr( get_the_date( 'c' ) ),
      esc_html( get_the_date() )
    );


    //
    // Categories.
    //

    if ( get_post_type() == 'x-portfolio' ) {
      if ( has_term( '', 'portfolio-category', NULL ) ) {
        $categories        = get_the_terms( get_the_ID(), 'portfolio-category' );
        $separator         = ', ';
        $categories_output = '';
        foreach ( $categories as $category ) {
          $categories_output .= '<a href="'
                              . get_term_link( $category->slug, 'portfolio-category' )
                              . '" title="'
                              . esc_attr( sprintf( __( "View all posts in: &ldquo;%s&rdquo;", '__x__' ), $category->name ) )
                              . '"> '
                              . $category->name
                              . '</a>'
                              . $separator;
        }

        $categories_list = sprintf( '<span>%s</span>',
          trim( $categories_output, $separator )
        );
      } else {
        $categories_list = '';
      }
    } else {
      $categories        = get_the_category();
      $separator         = ', ';
      $categories_output = '';
      foreach ( $categories as $category ) {
        $categories_output .= '<a href="'
                            . get_category_link( $category->term_id )
                            . '" title="'
                            . esc_attr( sprintf( __( "View all posts in: &ldquo;%s&rdquo;", '__x__' ), $category->name ) )
                            . '">'
                            . $category->name
                            . '</a>'
                            . $separator;
      }

      $categories_list = sprintf( '<span>%s</span>',
        trim( $categories_output, $separator )
      );
    }


    //
    // Comments link.
    //

    if ( comments_open() ) {

      $title  = apply_filters( 'x_entry_meta_comments_title', get_the_title() );
      $link   = apply_filters( 'x_entry_meta_comments_link', get_comments_link() );
      $number = apply_filters( 'x_entry_meta_comments_number', get_comments_number() );

      if ( $number == 0 ) {
        $text = __( 'Leave a Comment' , '__x__' );
      } else if ( $number == 1 ) {
        $text = $number . ' ' . __( 'Comment' , '__x__' );
      } else {
        $text = $number . ' ' . __( 'Comments' , '__x__' );
      }

      $comments = sprintf( '<span><a href="%1$s" title="%2$s" class="meta-comments">%3$s</a></span>',
        esc_url( $link ),
        esc_attr( sprintf( __( 'Leave a comment on: &ldquo;%s&rdquo;', '__x__' ), $title ) ),
        $text
      );

    } else {

      $comments = '';

    }


    //
    // Output.
    //

    if ( x_does_not_need_entry_meta() ) {
      return;
    } else {
      // printf( '<p class="p-meta">%1$s%2$s%3$s%4$s</p>test',
      //   $author,
      //   $date,
      //   $categories_list,
      //   $comments
      // );
      printf( '<p class="p-meta">by: %1$s%2$s%3$s</p>',
        $author,
        $date,
        $comments
      );
    }

  }
endif;

function generateCode($length=6, $switch=1) {
  switch ($switch){
    case 1:
      $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
      break;
    case 2: 
      $chars = "abcdefghijklmnopqrstuvwxyz";
      break;
    case 3:
      $chars = "0123456789";
      break;
    case 4:
      $chars = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIKLMNOPQRSTUVWXYZ";
      break;
    default:
      $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
      break;
  }
  $code = "";
  $clen = strlen($chars) - 1;  //a variable with the fixed length of chars correct for the fence post issue
  while (strlen($code) < $length) {
      $code .= $chars[mt_rand(0,$clen)];  //mt_rand's range is inclusive - this is why we need 0 to n-1
  }
  return $code;
}



#CODE START FOR COOKIE

function my_setcookie() {
    //$mwc = $_GET['mwc']; //
    $mwc = $_GET['pr'];
    if(!empty($mwc)){
      if(!isset($_COOKIE['emarky_uvmwc'])){
          $rand = generateCode();
          $mwc = $rand;
          define('UPCOOK',$mwc);
          setcookie( 'emarky_uvmwc', $mwc, time() + (10 * 365 * 24 * 60 * 60), COOKIEPATH, COOKIE_DOMAIN );  
      }      
    }

}

add_action( 'init', 'my_setcookie', 1);
#CODE END FOR COOKIE

function remove_revslider_meta_tag() {
 
    return '';
    
}
 
add_filter( 'revslider_meta_generator', 'remove_revslider_meta_tag', 1 );


// function modify_jquery() {
//   // if (is_admin()) {
//     wp_deregister_script('jquery');
//     wp_register_script('jquery', 'https://code.jquery.com/jquery-1.11.3.min.js');
//     wp_enqueue_script('jquery');
//   // }
// }
// add_action('init', 'modify_jquery');


// https://mekshq.com/remove-archives-wordpress-improve-seo/
// https://ryanbenhase.com/remove-tags-categories-or-any-taxonomy/
/* Register template redirect action callback */

//add_action('template_redirect', 'meks_remove_wp_archives');
 
/* Remove archives *//*
function meks_remove_wp_archives(){
  //If we are on category or tag or date or author archive
  if( is_category() || is_tag() || is_date() || is_author() ) {
    global $wp_query;
    $wp_query->set_404(); //set to 404 not found page
  }
}*/