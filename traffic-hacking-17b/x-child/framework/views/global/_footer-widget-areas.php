<?php

// =============================================================================
// VIEWS/GLOBAL/_FOOTER-WIDGET-AREAS.PHP
// -----------------------------------------------------------------------------
// Outputs the widget areas for the footer.
// =============================================================================

$n = x_footer_widget_areas_count();

//added by markramos
if(is_home()){  
?>
<div id="x-section-5" class="x-section bg-image" style=" margin: 0px 0px 0px 0px; padding: 45px 0px 0px 0px; background-image: url(http://upviral.com/wp-content/uploads/2015/09/footer-bg.jpg); background-color: transparent;" data-x-element="section" data-x-params="{&quot;type&quot;:&quot;image&quot;,&quot;parallax&quot;:false}"><div class="x-container max width" style="margin: 0px auto 0px auto; padding: 0px 0px 0px 0px; "><div class="x-column x-sm center-text  x-1-1" style="padding: 0px 0px 0px 0px; "><h2 class="h-custom-headline h3" style="color: #fff;"><span><strong>Get Instant Access To UpViral</strong></span></h2><hr class="x-gap" style="margin: 20px 0 0 0;"><a class="x-btn x-btn-jumbo" href="http://upviral.com/pricing/" data-options="thumbnail: ''">Get UpViral Now</a><hr class="x-gap" style="margin: 50px 0 0 0;"><div class="x-text"><p><a class="box-light" href="http://upviral.com/learn-more/">Learn More</a> <a class="box-light" href="http://upviral.com/how-to-use-upviral/">How To Use UpViral</a> <a class="box-light" href="http://upviral.com/faq/">FAQ</a></p>
</div><hr class="x-gap" style="margin: 50px 0 0 0;"><img class="x-img x-img-none" style="margin-bottom: 0px;" src="http://upviral.com/wp-content/uploads/2015/09/separator.png"></div></div></div>
<style>
.box-light {
padding: 10px 20px;
border: 1px solid #69717d;
border-radius: 3px;
color: #fff;
font-size: 16px;
margin-top: 20px !important;
margin-bottom: 20px !important;
margin-left: 5px !important;
margin-right: 5px !important;
}

.box-light:hover {
color:#384354 ;
background-color:rgba(255,255,255,.8);
}
</style>
<?php
}
//===========================
?>

<?php if ( $n != 0 ) : ?>

  <footer class="x-colophon top" role="contentinfo">
    <div class="x-container max width">

      <?php

      $i = 0; while ( $i < $n ) : $i++;

        $last = ( $i == $n ) ? ' last' : '';

        echo '<div class="x-column x-md x-1-' . $n . $last . '">';
          dynamic_sidebar( 'footer-' . $i );
        echo '</div>';

      endwhile;

      ?>

    </div>
  </footer>

<?php endif; ?>