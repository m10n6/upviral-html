<?php

// =============================================================================
// VIEWS/RENEW/CONTENT-PORTFOLIO.PHP
// -----------------------------------------------------------------------------
// Portfolio post output for Renew.
// =============================================================================

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


  <div class="entry-wrap cf">

    <?php if ( is_singular() ) : ?>

      <div class="entry-info">
        <?php //x_get_view( 'global', '_content', 'integration' ); 
          the_content();
        ?>

      </div>

    <?php endif; ?>

  </div>
</article>