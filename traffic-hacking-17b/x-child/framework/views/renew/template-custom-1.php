<?php

// =============================================================================
// VIEWS/RENEW/TEMPLATE-CUSTOM-1.PHP (No Container | No Header, Footer Socket)
// -----------------------------------------------------------------------------
// A blank page for creating unique layouts.
// =============================================================================

if ( apply_filters( 'x_legacy_cranium_headers', true ) ) {
  x_get_view( 'global', '_header' );
} else {
  get_header();
}

?>

  <div class="x-main full" role="main">

    <?php while ( have_posts() ) : the_post(); ?>

      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <?php x_get_view( 'global', '_content', 'the-content' ); ?>
      </article>

    <?php endwhile; ?>

  </div>

<?php //

echo '
<footer class="x-colophon bottom" role="contentinfo">
      <div class="x-container max width">

        
        
        <!-- added by mark -->
        <div id="footer-logo">
          <img src="https://upviral.com/wp-content/themes/x-child/images/logo-on-dark.png"  style="visibility: hidden;">
        </div>
        <div id="footer-terms">
                      <!-- <div class="x-colophon-content"> -->
              <p>
Copyright © 2018 Emarky B.V. All rights reserved.<br>
<a href=" https://upviral.com/terms-of-service/">Terms of Service</a> - <a href="https://upviral.com/privacy-policy">Privacy Policy</a> - <a href="http://support.upviral.com">Contact</a>
</p>            <!-- </div> -->
                  </div>
      </div>
</footer>
';

if ( apply_filters( 'x_legacy_cranium_footers', true ) ) {
  x_get_view( 'global', '_footer' );
} else {
  get_footer();
}

?>

