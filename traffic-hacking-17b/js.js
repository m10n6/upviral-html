jQuery(document).ready(function(){
  jQuery('#btnMCSubmitWidget').hide();
  jQuery('#btnMCSubmitWidget2').hide();
  jQuery('#btnWatchVideo').click(function(){
		//jQuery('#btnOptinOverlay')[0].click();  
    //MC.getWidget(2332532).submit();
    jQuery('#btnWebinarRegister')[0].click(); 
/*     jQuery('#btnMCSubmitWidget').trigger('click'); */
  });
  
  jQuery('#btnSendFreeVideo').click(function(e){
    e.stopPropagation();
    jQuery('#btnMainClaimSeat')[0].click(); 
    return false;
  });
  
  
  
  jQuery('.reg_now').click(function(e){
    e.stopPropagation();
    jQuery('#btnSendFreeVideo')[0].click(); 
    return false;
  });
  
});

function getSchedule(){
  var GMTDate = new Date();
  var d = GMTDate.toString().split(' ');
  jQuery.ajax({
    type : 'post',
    url : '//upviral.com/jam-scripts/get-schedule-40ad0e653a.php',
    data : {
      'gmt_offset' : d[5]
    },
    success : function(res){
      console.log('testGetSched');
      console.log(res);
      jQuery('#webinar_dt').append(res);
      jQuery('#tz_info').append('<br/>' + d[5]);
      var tz_ = show_timezone_info();
      var ctr = 0;
      jQuery('#webinar_dt option').each(function(){
        if(ctr > 0){
          jQuery(this).append(' (' + tz_ + ')');  
        }
        ctr++;
      });
      jQuery('#actDate').html(jQuery('#webinar_dt option:selected').text());
    }
  })
}

getSchedule();


var loader_ = '<div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>';
jQuery('#webinar_dt').change(function(e){
      e.stopPropagation();
      console.log(jQuery(this).val());
      var val_ = jQuery(this).val();
      jQuery('#divDTime').append(loader_);
      setTimeout(function(){
        jQuery('#divDTime .lds-spinner').remove();
        if(val_ !== '-'){
          jQuery('.hideform').show();
        }else{
          jQuery('.hideform').hide();
        }
        jQuery('#webinar_dt option[value="-"]').remove();

      }, 5000);
});


function checkReqField(thisField){
  var this_ = thisField.val();
  if(this_ == ''){
    thisField.addClass('required_field');
  }else{
    thisField.removeClass('required_field');
  }
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

jQuery('#inpEmail').blur(function(){
  checkReqField(jQuery(this));
});

jQuery('#inpName').blur(function(){
  checkReqField(jQuery(this));
});

jQuery('#inpName').keypress(function (e) {
 var key = e.which;
 if(key == 13){
    jQuery('#btnReserveSeat')[0].click(); 
    return false;  
  }
}); 

jQuery('#inpEmail').keypress(function (e) {
 var key = e.which;
 if(key == 13){
    jQuery('#btnReserveSeat')[0].click(); 
    return false;  
  }
}); 


 

jQuery('#btnReserveSeat').click(function(){

  dataLayer.push({'event' : 'webinar-enter-form'});

  var webinar_dt = jQuery('#webinar_dt').val();
  var inpEmail = jQuery('#inpEmail').val();
  var inpName = jQuery('#inpName').val();
  if(inpName !== '' && inpEmail !== ''){
    if(validateEmail(inpEmail)){
      jQuery.ajax({
        type : 'post',
        url : '//upviral.com/jam-scripts/submit-40ad0e653a.php',
        data : {
          'webinar_dt':webinar_dt,
          'inpEmail':inpEmail,
          'inpName':inpName
        },
        success : function(res){
          console.log(res);
          jQuery('.w3-blue').attr('style', 'width: 100%');
          jQuery('.w3-blue').html('100%');
          //window.location = res;
          ga('create', 'UA-62842650-1', 'auto', {'allowLinker': true});
          ga('require', 'linker');
          ga('linker:autoLink', ['upviral.com','connectio.io','zaxaa.com', 'emarky.zaxaa.com', 'app.webinarjam.net', 'emarky.thrivecart.com', 'events.genndi.com'], true);
           ga(function(tracker) {
              var linkerParam = tracker.get('linkerParam');

              var destinationLink = res + '&' + linkerParam;
              window.location = destinationLink;
          });
        }
      });  
    }else{
      swal({
        title: "Error Sending...",
        text: "Please check email format!",
        icon: "error",
        dangerMode: true,
      });    
    }
   
  }else{
    checkReqField(jQuery('#inpName'));
    checkReqField(jQuery('#inpEmail'));
    swal({
      title: "Error Sending...",
      text: "Please fill-in all fields!",
      icon: "error",
      dangerMode: true,
    });

  }

});