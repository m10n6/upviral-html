<?php
	include('config-687b8463a5.php');
	$ch = curl_init();
	$header = array("User-Agent: " . $_SERVER["HTTP_USER_AGENT"], "Content-Type: application/x-www-form-urlencoded");

	$todayreg = date("Y-m-d");

	$url_register = 'https://webinarjam.genndi.com/api/everwebinar/register';
	$url_sched = 'https://webinarjam.genndi.com/api/everwebinar/webinar';

	curl_setopt($ch, CURLOPT_URL, $url_sched);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);

	$timezone = $_POST['gmt_offset'];
	// $timezone = 'GMT+01:00';
	$len_timezone = strlen($timezone);
	// echo $len.'<br>';
	$timezone = substr($timezone, 0, ($len_timezone-2)).':'.substr($timezone, -2, $len_timezone);
	// curl_setopt($ch, CURLOPT_POSTFIELDS,
	//             "api_key=".$api_key."&webinar_id=".$webinar_id."&first_name=".$namereg."&email=".$emailreg."&schedule=".$schedule."&real_dates=1");
	curl_setopt($ch, CURLOPT_POSTFIELDS,
	            "api_key=".$api_key."&webinar_id=".$webinar_id."&timezone=".$timezone."&real_dates=1
	            ");//&selected_date=".$todayreg."&to_timezone=America/New_York&from_timezone=America/New_York&tz_selected=1");
	// curl_setopt($ch, CURLOPT_POSTFIELDS,
	//             "webicode=".$webicode."&memberid=".$memberid."&page=registration&page_tag=formregistration&userid=0&first_name=".$namereg."&email_address=".$emailreg."&selected_date=".$todayreg."&selected_schedule=jot&to_timezone=America/New_York&from_timezone=America/New_York&tz_selected=1");
				
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	$server_output = curl_exec ($ch);
	curl_close ($ch);
		$json_encoded_serveroutput = json_decode($server_output);
		 //echo '<pre>';
		 //print_r($json_encoded_serveroutput);
		 //echo '</pre>';
		if($json_encoded_serveroutput->status == 'success'){

			$selection = 'selected';
			foreach ($json_encoded_serveroutput->webinar->schedules as $sched) {
				# code...
				echo '<option value="'.$sched->schedule.'" '.$selection.'>'.$sched->date.'</option>';	
				$selection = '';
			}
		}
?>