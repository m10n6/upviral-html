			jQuery(document).ready(function(){
				jQuery("#documentor-1").documentor({
					documentid	: 1,
					docid		: "documentor-1",
					animation	: "",
					indexformat	: "1",
					pformat		: "decimal",
					cformat		: "decimal",					
					secstyle	: "",
					actnavbg_default: "1",
					actnavbg_color	: "#0093bf",
					scrolling	: "1",
					fixmenu		: "1",
					skin		: "default",
					scrollBarSize	: "3",
					scrollBarColor	: "#f45349",
					scrollBarOpacity: "0.4",
					windowprint	: "0",
					menuTop: "0",
					socialshare	: 0,
					sharecount	: 1,
					fbshare		: 1,
					twittershare	: 1,
					gplusshare	: 1,
					pinshare	: 1,
					togglechild	: 0,
					noResultsStr: "No results found!",
				});	
			});
			var editor_139 = ace.edit( 'code_snippet_101_1' );
            var session_139 = editor_139.getSession();
            
            editor_139.setTheme( 'ace/theme/tomorrow_night' );
            session_139.setMode( 'ace/mode/php' );
            
            editor_139.setReadOnly( true );
            editor_139.setHighlightActiveLine( false );
            editor_139.setShowPrintMargin( false );
            editor_139.setHighlightGutterLine( false );
            
            var doc_139 = session_139.getDocument();
            var lines = parseInt( doc_139.getLength() );
            var line_height = 20;
            var editor_height = lines * line_height;
            jQuery( '#code_snippet_101_1' ).height( editor_height + 'px' );
            
            
            jQuery( window ).load( function(e) {
                var doc_139 = session_139.getDocument();
                var lines = parseInt( doc_139.getLength() );
                var line_height = jQuery( '#code_snippet_101_1 .ace_line' ).height();
            
                var editor_height = lines * line_height;
                jQuery( '#code_snippet_101_1' ).height( editor_height + 'px' );
                jQuery( '#code_snippet_101_1 .ace_scroller' ).height( editor_height + 'px' );
                jQuery( '#code_snippet_101_1 .ace_gutter' ).height( editor_height + 'px' );
            
                var content_height = editor_height + ( line_height * 2 );
                jQuery( '#code_snippet_101_1 .ace_content' ).height( content_height + 'px' );
            });

             var editor_237 = ace.edit( 'code_snippet_101_2' );
            var session_237 = editor_237.getSession();
            editor_237.setTheme( 'ace/theme/tomorrow_night' );
            session_237.setMode( 'ace/mode/json' );
            
            editor_237.setReadOnly( true );
            editor_237.setHighlightActiveLine( false );
            editor_237.setShowPrintMargin( false );
            editor_237.setHighlightGutterLine( false );
            
            var doc_237 = session_237.getDocument();
            var lines = parseInt( doc_237.getLength() );
            var line_height = 20;
            var editor_height = lines * line_height;
            jQuery( '#code_snippet_101_2' ).height( editor_height + 'px' );
            
            
            jQuery( window ).load( function(e) {
                var doc_237 = session_237.getDocument();
                var lines = parseInt( doc_237.getLength() );
                var line_height = jQuery( '#code_snippet_101_2 .ace_line' ).height();
            
                var editor_height = lines * line_height;
                jQuery( '#code_snippet_101_2' ).height( editor_height + 'px' );
                jQuery( '#code_snippet_101_2 .ace_scroller' ).height( editor_height + 'px' );
                jQuery( '#code_snippet_101_2 .ace_gutter' ).height( editor_height + 'px' );
            
                var content_height = editor_height + ( line_height * 2 );
                jQuery( '#code_snippet_101_2 .ace_content' ).height( content_height + 'px' );
            });
            
             var editor_217 = ace.edit( 'code_snippet_102_1' );
            var session_217 = editor_217.getSession();
            
            editor_217.setTheme( 'ace/theme/tomorrow_night' );
            session_217.setMode( 'ace/mode/php' );
            
            editor_217.setReadOnly( true );
            editor_217.setHighlightActiveLine( false );
            editor_217.setShowPrintMargin( false );
            editor_217.setHighlightGutterLine( false );
            
            var doc_217 = session_217.getDocument();
            var lines = parseInt( doc_217.getLength() );
            var line_height = 20;
            var editor_height = lines * line_height;
            jQuery( '#code_snippet_102_1' ).height( editor_height + 'px' );
            
            
            jQuery( window ).load( function(e) {
                var doc_217 = session_217.getDocument();
                var lines = parseInt( doc_217.getLength() );
                var line_height = jQuery( '#code_snippet_102_1 .ace_line' ).height();
            
                var editor_height = lines * line_height;
                jQuery( '#code_snippet_102_1' ).height( editor_height + 'px' );
                jQuery( '#code_snippet_102_1 .ace_scroller' ).height( editor_height + 'px' );
                jQuery( '#code_snippet_102_1 .ace_gutter' ).height( editor_height + 'px' );
            
                var content_height = editor_height + ( line_height * 2 );
                jQuery( '#code_snippet_102_1 .ace_content' ).height( content_height + 'px' );
            });
            
            var editor_218 = ace.edit( 'code_snippet_102_2' );
            var session_218 = editor_218.getSession();
            
            editor_218.setTheme( 'ace/theme/tomorrow_night' );
            session_218.setMode( 'ace/mode/json' );
            
            editor_218.setReadOnly( true );
            editor_218.setHighlightActiveLine( false );
            editor_218.setShowPrintMargin( false );
            editor_218.setHighlightGutterLine( false );
            
            var doc_218 = session_218.getDocument();
            var lines = parseInt( doc_218.getLength() );
            var line_height = 20;
            var editor_height = lines * line_height;
            jQuery( '#code_snippet_102_2' ).height( editor_height + 'px' );
            
            
            jQuery( window ).load( function(e) {
                var doc_218 = session_218.getDocument();
                var lines = parseInt( doc_218.getLength() );
                var line_height = jQuery( '#code_snippet_102_2 .ace_line' ).height();
            
                var editor_height = lines * line_height;
                jQuery( '#code_snippet_102_2' ).height( editor_height + 'px' );
                jQuery( '#code_snippet_102_2 .ace_scroller' ).height( editor_height + 'px' );
                jQuery( '#code_snippet_102_2 .ace_gutter' ).height( editor_height + 'px' );
            
                var content_height = editor_height + ( line_height * 2 );
                jQuery( '#code_snippet_102_2 .ace_content' ).height( content_height + 'px' );
            });

             
            
            

            var editor_139 = ace.edit( 'code_snippet_104_1' );
            var session_139 = editor_139.getSession();
            
            editor_139.setTheme( 'ace/theme/tomorrow_night' );
            session_139.setMode( 'ace/mode/php' );
            
            editor_139.setReadOnly( true );
            editor_139.setHighlightActiveLine( false );
            editor_139.setShowPrintMargin( false );
            editor_139.setHighlightGutterLine( false );
            
            var doc_139 = session_139.getDocument();
            var lines = parseInt( doc_139.getLength() );
            var line_height = 20;
            var editor_height = lines * line_height;
            jQuery( '#code_snippet_104_1' ).height( editor_height + 'px' );
            
            
            jQuery( window ).load( function(e) {
                var doc_139 = session_139.getDocument();
                var lines = parseInt( doc_139.getLength() );
                var line_height = jQuery( '#code_snippet_104_1 .ace_line' ).height();
            
                var editor_height = lines * line_height;
                jQuery( '#code_snippet_104_1' ).height( editor_height + 'px' );
                jQuery( '#code_snippet_104_1 .ace_scroller' ).height( editor_height + 'px' );
                jQuery( '#code_snippet_104_1 .ace_gutter' ).height( editor_height + 'px' );
            
                var content_height = editor_height + ( line_height * 2 );
                jQuery( '#code_snippet_104_1 .ace_content' ).height( content_height + 'px' );
            });
            
             var editor_237 = ace.edit( 'code_snippet_104_2' );
            var session_237 = editor_237.getSession();
            editor_237.setTheme( 'ace/theme/tomorrow_night' );
            session_237.setMode( 'ace/mode/json' );
            
            editor_237.setReadOnly( true );
            editor_237.setHighlightActiveLine( false );
            editor_237.setShowPrintMargin( false );
            editor_237.setHighlightGutterLine( false );
            
            var doc_237 = session_237.getDocument();
            var lines = parseInt( doc_237.getLength() );
            var line_height = 20;
            var editor_height = lines * line_height;
            jQuery( '#code_snippet_104_2' ).height( editor_height + 'px' );
            
            
            jQuery( window ).load( function(e) {
                var doc_237 = session_237.getDocument();
                var lines = parseInt( doc_237.getLength() );
                var line_height = jQuery( '#code_snippet_104_2 .ace_line' ).height();
            
                var editor_height = lines * line_height;
                jQuery( '#code_snippet_104_2' ).height( editor_height + 'px' );
                jQuery( '#code_snippet_104_2 .ace_scroller' ).height( editor_height + 'px' );
                jQuery( '#code_snippet_104_2 .ace_gutter' ).height( editor_height + 'px' );
            
                var content_height = editor_height + ( line_height * 2 );
                jQuery( '#code_snippet_104_2 .ace_content' ).height( content_height + 'px' );
            });

            var editor_139 = ace.edit( 'code_snippet_105_1' );
            var session_139 = editor_139.getSession();
            
            editor_139.setTheme( 'ace/theme/tomorrow_night' );
            session_139.setMode( 'ace/mode/php' );
            
            editor_139.setReadOnly( true );
            editor_139.setHighlightActiveLine( false );
            editor_139.setShowPrintMargin( false );
            editor_139.setHighlightGutterLine( false );
            
            var doc_139 = session_139.getDocument();
            var lines = parseInt( doc_139.getLength() );
            var line_height = 20;
            var editor_height = lines * line_height;
            jQuery( '#code_snippet_105_1' ).height( editor_height + 'px' );
            
            
            jQuery( window ).load( function(e) {
                var doc_139 = session_139.getDocument();
                var lines = parseInt( doc_139.getLength() );
                var line_height = jQuery( '#code_snippet_105_1 .ace_line' ).height();
            
                var editor_height = lines * line_height;
                jQuery( '#code_snippet_105_1' ).height( editor_height + 'px' );
                jQuery( '#code_snippet_105_1 .ace_scroller' ).height( editor_height + 'px' );
                jQuery( '#code_snippet_105_1 .ace_gutter' ).height( editor_height + 'px' );
            
                var content_height = editor_height + ( line_height * 2 );
                jQuery( '#code_snippet_105_1 .ace_content' ).height( content_height + 'px' );
            });

            var editor_237 = ace.edit( 'code_snippet_105_2' );
            var session_237 = editor_237.getSession();
            editor_237.setTheme( 'ace/theme/tomorrow_night' );
            session_237.setMode( 'ace/mode/json' );
            
            editor_237.setReadOnly( true );
            editor_237.setHighlightActiveLine( false );
            editor_237.setShowPrintMargin( false );
            editor_237.setHighlightGutterLine( false );
            
            var doc_237 = session_237.getDocument();
            var lines = parseInt( doc_237.getLength() );
            var line_height = 20;
            var editor_height = lines * line_height;
            jQuery( '#code_snippet_105_2' ).height( editor_height + 'px' );
            
            
            jQuery( window ).load( function(e) {
                var doc_237 = session_237.getDocument();
                var lines = parseInt( doc_237.getLength() );
                var line_height = jQuery( '#code_snippet_105_2 .ace_line' ).height();
            
                var editor_height = lines * line_height;
                jQuery( '#code_snippet_105_2' ).height( editor_height + 'px' );
                jQuery( '#code_snippet_105_2 .ace_scroller' ).height( editor_height + 'px' );
                jQuery( '#code_snippet_105_2 .ace_gutter' ).height( editor_height + 'px' );
            
                var content_height = editor_height + ( line_height * 2 );
                jQuery( '#code_snippet_105_2 .ace_content' ).height( content_height + 'px' );
            });
            
            var editor_139 = ace.edit( 'code_snippet_106_1' );
            var session_139 = editor_139.getSession();
            
            editor_139.setTheme( 'ace/theme/tomorrow_night' );
            session_139.setMode( 'ace/mode/php' );
            
            editor_139.setReadOnly( true );
            editor_139.setHighlightActiveLine( false );
            editor_139.setShowPrintMargin( false );
            editor_139.setHighlightGutterLine( false );
            
            var doc_139 = session_139.getDocument();
            var lines = parseInt( doc_139.getLength() );
            var line_height = 20;
            var editor_height = lines * line_height;
            jQuery( '#code_snippet_106_1' ).height( editor_height + 'px' );
            
            
            jQuery( window ).load( function(e) {
                var doc_139 = session_139.getDocument();
                var lines = parseInt( doc_139.getLength() );
                var line_height = jQuery( '#code_snippet_106_1 .ace_line' ).height();
            
                var editor_height = lines * line_height;
                jQuery( '#code_snippet_106_1' ).height( editor_height + 'px' );
                jQuery( '#code_snippet_106_1 .ace_scroller' ).height( editor_height + 'px' );
                jQuery( '#code_snippet_106_1 .ace_gutter' ).height( editor_height + 'px' );
            
                var content_height = editor_height + ( line_height * 2 );
                jQuery( '#code_snippet_106_1 .ace_content' ).height( content_height + 'px' );
            });

             var editor_237 = ace.edit( 'code_snippet_106_2' );
            var session_237 = editor_237.getSession();
            editor_237.setTheme( 'ace/theme/tomorrow_night' );
            session_237.setMode( 'ace/mode/json' );
            
            editor_237.setReadOnly( true );
            editor_237.setHighlightActiveLine( false );
            editor_237.setShowPrintMargin( false );
            editor_237.setHighlightGutterLine( false );
            
            var doc_237 = session_237.getDocument();
            var lines = parseInt( doc_237.getLength() );
            var line_height = 20;
            var editor_height = lines * line_height;
            jQuery( '#code_snippet_106_2' ).height( editor_height + 'px' );
            
            
            jQuery( window ).load( function(e) {
                var doc_237 = session_237.getDocument();
                var lines = parseInt( doc_237.getLength() );
                var line_height = jQuery( '#code_snippet_106_2 .ace_line' ).height();
            
                var editor_height = lines * line_height;
                jQuery( '#code_snippet_106_2' ).height( editor_height + 'px' );
                jQuery( '#code_snippet_106_2 .ace_scroller' ).height( editor_height + 'px' );
                jQuery( '#code_snippet_106_2 .ace_gutter' ).height( editor_height + 'px' );
            
                var content_height = editor_height + ( line_height * 2 );
                jQuery( '#code_snippet_106_2 .ace_content' ).height( content_height + 'px' );
            });

              var editor_139 = ace.edit( 'code_snippet_107_1' );
            var session_139 = editor_139.getSession();
            
            editor_139.setTheme( 'ace/theme/tomorrow_night' );
            session_139.setMode( 'ace/mode/php' );
            
            editor_139.setReadOnly( true );
            editor_139.setHighlightActiveLine( false );
            editor_139.setShowPrintMargin( false );
            editor_139.setHighlightGutterLine( false );
            
            var doc_139 = session_139.getDocument();
            var lines = parseInt( doc_139.getLength() );
            var line_height = 20;
            var editor_height = lines * line_height;
            jQuery( '#code_snippet_107_1' ).height( editor_height + 'px' );
            
            
            jQuery( window ).load( function(e) {
                var doc_139 = session_139.getDocument();
                var lines = parseInt( doc_139.getLength() );
                var line_height = jQuery( '#code_snippet_107_1 .ace_line' ).height();
            
                var editor_height = lines * line_height;
                jQuery( '#code_snippet_107_1' ).height( editor_height + 'px' );
                jQuery( '#code_snippet_107_1 .ace_scroller' ).height( editor_height + 'px' );
                jQuery( '#code_snippet_107_1 .ace_gutter' ).height( editor_height + 'px' );
            
                var content_height = editor_height + ( line_height * 2 );
                jQuery( '#code_snippet_107_1 .ace_content' ).height( content_height + 'px' );
            });

            var editor_237 = ace.edit( 'code_snippet_107_2' );
            var session_237 = editor_237.getSession();
            editor_237.setTheme( 'ace/theme/tomorrow_night' );
            session_237.setMode( 'ace/mode/json' );
            
            editor_237.setReadOnly( true );
            editor_237.setHighlightActiveLine( false );
            editor_237.setShowPrintMargin( false );
            editor_237.setHighlightGutterLine( false );
            
            var doc_237 = session_237.getDocument();
            var lines = parseInt( doc_237.getLength() );
            var line_height = 20;
            var editor_height = lines * line_height;
            jQuery( '#code_snippet_107_2' ).height( editor_height + 'px' );
            
            
            jQuery( window ).load( function(e) {
                var doc_237 = session_237.getDocument();
                var lines = parseInt( doc_237.getLength() );
                var line_height = jQuery( '#code_snippet_107_2 .ace_line' ).height();
            
                var editor_height = lines * line_height;
                jQuery( '#code_snippet_107_2' ).height( editor_height + 'px' );
                jQuery( '#code_snippet_107_2 .ace_scroller' ).height( editor_height + 'px' );
                jQuery( '#code_snippet_107_2 .ace_gutter' ).height( editor_height + 'px' );
            
                var content_height = editor_height + ( line_height * 2 );
                jQuery( '#code_snippet_107_2 .ace_content' ).height( content_height + 'px' );
            });


            var editor_237 = ace.edit( 'code_snippet_205_2' );
            var session_237 = editor_237.getSession();
            editor_237.setTheme( 'ace/theme/tomorrow_night' );
            session_237.setMode( 'ace/mode/json' );
            
            editor_237.setReadOnly( true );
            editor_237.setHighlightActiveLine( false );
            editor_237.setShowPrintMargin( false );
            editor_237.setHighlightGutterLine( false );
            
            var doc_237 = session_237.getDocument();
            var lines = parseInt( doc_237.getLength() );
            var line_height = 20;
            var editor_height = lines * line_height;
            jQuery( '#code_snippet_205_2' ).height( editor_height + 'px' );
            
            
            jQuery( window ).load( function(e) {
                var doc_237 = session_237.getDocument();
                var lines = parseInt( doc_237.getLength() );
                var line_height = jQuery( '#code_snippet_205_2 .ace_line' ).height();
            
                var editor_height = lines * line_height;
                jQuery( '#code_snippet_205_2' ).height( editor_height + 'px' );
                jQuery( '#code_snippet_205_2 .ace_scroller' ).height( editor_height + 'px' );
                jQuery( '#code_snippet_205_2 .ace_gutter' ).height( editor_height + 'px' );
            
                var content_height = editor_height + ( line_height * 2 );
                jQuery( '#code_snippet_205_2 .ace_content' ).height( content_height + 'px' );
            });


             var editor_139 = ace.edit( 'code_snippet_205_1' );
            var session_139 = editor_139.getSession();
            
            editor_139.setTheme( 'ace/theme/tomorrow_night' );
            session_139.setMode( 'ace/mode/php' );
            
            editor_139.setReadOnly( true );
            editor_139.setHighlightActiveLine( false );
            editor_139.setShowPrintMargin( false );
            editor_139.setHighlightGutterLine( false );
            
            var doc_139 = session_139.getDocument();
            var lines = parseInt( doc_139.getLength() );
            var line_height = 20;
            var editor_height = lines * line_height;
            jQuery( '#code_snippet_205_1' ).height( editor_height + 'px' );
            
            
            jQuery( window ).load( function(e) {
                var doc_139 = session_139.getDocument();
                var lines = parseInt( doc_139.getLength() );
                var line_height = jQuery( '#code_snippet_205_1 .ace_line' ).height();
            
                var editor_height = lines * line_height;
                jQuery( '#code_snippet_205_1' ).height( editor_height + 'px' );
                jQuery( '#code_snippet_205_1 .ace_scroller' ).height( editor_height + 'px' );
                jQuery( '#code_snippet_205_1 .ace_gutter' ).height( editor_height + 'px' );
            
                var content_height = editor_height + ( line_height * 2 );
                jQuery( '#code_snippet_205_1 .ace_content' ).height( content_height + 'px' );
            });

