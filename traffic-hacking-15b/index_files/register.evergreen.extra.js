wj_jquery_version = '1.12.4';
js_jquery_894430ec1b1fa701650764dbf3b51b06c9011f10 = document.createElement('script');
js_jquery_894430ec1b1fa701650764dbf3b51b06c9011f10.type = 'text/javascript';
js_jquery_894430ec1b1fa701650764dbf3b51b06c9011f10.onload = function() {

    jquery_894430ec1b1fa701650764dbf3b51b06c9011f10 = jQuery.noConflict(true);

    var css = '.wj-registration-modal-opened{overflow-y:hidden;}',
        head = document.head || document.getElementsByTagName('head')[0],
        style = document.createElement('style'),
        iframeOffsetHeight = 0,
        iframeOffsetTop = 0;

    style.type = 'text/css';
    if (style.styleSheet){
        style.styleSheet.cssText = css;
    } else {
        style.appendChild(document.createTextNode(css));
    }

    head.appendChild(style);

    var resize = function() {
        if (document.getElementById('regpopFrame') === null) {
            return false;
        }

        var receiver = document.getElementById('regpopFrame').contentWindow;

        receiver.postMessage({"a":"getOffsetHeight"}, '*');
        receiver.postMessage({"a":"getOffsetTop"}, '*');
        setTimeout(realResize, 50);
    }

    realResize = function() {
        var windowHeight3 = window.innerHeight;
        jquery_894430ec1b1fa701650764dbf3b51b06c9011f10('#regpopFrame').height(windowHeight3).fadeIn();
    }

    var onMessage = function(messageEvent) {
        if (messageEvent.data.a == 'resize') {
            resize();
        }
        if (messageEvent.data.a == 'setOffsetHeight') {
            iframeOffsetHeight = messageEvent.data.v;
        }
        if (messageEvent.data.a == 'setOffsetTop') {
            iframeOffsetTop = messageEvent.data.v;
        }
        if (messageEvent.data.a == 'close') {
            jquery_894430ec1b1fa701650764dbf3b51b06c9011f10("#frameContent").fadeOut();
            jquery_894430ec1b1fa701650764dbf3b51b06c9011f10('#regpopFrame').fadeOut();
            jquery_894430ec1b1fa701650764dbf3b51b06c9011f10("body").removeClass('wj-registration-modal-opened');
        }
        // if (messageEvent.data.a == 'redirect') {
        //     window.parent.location = messageEvent.data.url;
        // }
    }

    window.onload = function() {
        window.addEventListener('message', onMessage, false);
        window.addEventListener("resize", resize, false);
    }

    jquery_894430ec1b1fa701650764dbf3b51b06c9011f10( document ).ready(function(jquery_894430ec1b1fa701650764dbf3b51b06c9011f10) {

        var odiv=jquery_894430ec1b1fa701650764dbf3b51b06c9011f10("<div>").attr("id","frameContent").css({"webkit-overflow-scrolling":"touch !important", "overflow-y":"hidden", "top":0, "left":0, "background":"rgba(0,0,0,0.8)", "position":"fixed", "width":"100%", "height":"100%", "display":"none", "z-index":2147483647});
        var div1 = jquery_894430ec1b1fa701650764dbf3b51b06c9011f10("<div>").attr("id","div-wj-1").css({"width":"100%", "height": "100%", "position": "relative"});
        var div2 = jquery_894430ec1b1fa701650764dbf3b51b06c9011f10("<div>").attr("id","div-wj-2").css({"margin":"auto", "height": "100%"});
        div1.append(div2);
        odiv.append(div1);
        var itz=jquery_894430ec1b1fa701650764dbf3b51b06c9011f10("<input>").attr("type","hidden").attr("name","local_timezone");
        var regbut = jquery_894430ec1b1fa701650764dbf3b51b06c9011f10("body").find("[title^='regpopbox']:first");
        if (regbut.length == 1) {
            var wjs_w = regbut.attr('title').split('_')[2];
            var wjs_m = regbut.attr('title').split('_')[1];
        }

        var d = new Date();
        var offset = -d.getTimezoneOffset();
        // default timezone
        jquery_894430ec1b1fa701650764dbf3b51b06c9011f10(itz).val('America/New_York');
        var tz_xhr = jquery_894430ec1b1fa701650764dbf3b51b06c9011f10.post('https://events.genndi.com/register/detectTimezone', {offset: offset});
        tz_xhr.done(function(data){
            jquery_894430ec1b1fa701650764dbf3b51b06c9011f10(itz).val(data.timezone);
        });

        if (jquery_894430ec1b1fa701650764dbf3b51b06c9011f10("#frameContent").length > 0) {
            // clean up legacy codes out there
            // data-wjs-required is used on login pages
            if (jquery_894430ec1b1fa701650764dbf3b51b06c9011f10("#frameContent").attr('data-wjs-required') != 1) {
                jquery_894430ec1b1fa701650764dbf3b51b06c9011f10("#frameContent").remove();
            }
        }


        jquery_894430ec1b1fa701650764dbf3b51b06c9011f10("body").append(odiv, itz);

        var onClickEvent = function() {
            var titleValue = jquery_894430ec1b1fa701650764dbf3b51b06c9011f10(this).attr('title');
            if (titleValue != "" && typeof titleValue !== "undefined") {
                var addressValue = titleValue.split('_')[0];
                if (typeof page === 'undefined' || typeof page === 'object') {
                    page = '';
                }
                if (addressValue == 'regpopbox') {
                    var memberid = titleValue.split('_')[1];
                    var webicode = titleValue.split('_')[2];
                    var timezone = jquery_894430ec1b1fa701650764dbf3b51b06c9011f10("input[name=local_timezone]").val();
                    if (jquery_894430ec1b1fa701650764dbf3b51b06c9011f10.trim(addressValue) != '' && jquery_894430ec1b1fa701650764dbf3b51b06c9011f10.trim(memberid) != '' && jquery_894430ec1b1fa701650764dbf3b51b06c9011f10.trim(webicode) != '') {
                        jquery_894430ec1b1fa701650764dbf3b51b06c9011f10("body").addClass('wj-registration-modal-opened');

                        var url = "https://events.genndi.com/registerBoxEvergreen/"+memberid+"/"+webicode+"?page="+page+"&page_tag=formregistration&ts="+Date.now();

                        if (typeof timezone === "string") {
                            url = url.concat("&tz="+timezone);
                        }

                        jquery_894430ec1b1fa701650764dbf3b51b06c9011f10("#regpopFrame").remove();
                        if (jquery_894430ec1b1fa701650764dbf3b51b06c9011f10("#regpopFrame").length == 0) {
                            jquery_894430ec1b1fa701650764dbf3b51b06c9011f10('<iframe>', {
                                src: url,
                                id:  'regpopFrame',
                                style: 'display:none;',
                                frameborder: 0,
                                name: 'regpopFrame',
                                scrolling: 'no'
                            }).appendTo(jquery_894430ec1b1fa701650764dbf3b51b06c9011f10("#div-wj-2"));
                        }
                        jquery_894430ec1b1fa701650764dbf3b51b06c9011f10('#regpopFrame').hide();
                        jquery_894430ec1b1fa701650764dbf3b51b06c9011f10('#regpopFrame').css({'width':'100%', 'z-index':250});
                        jquery_894430ec1b1fa701650764dbf3b51b06c9011f10("#frameContent").fadeIn('fast', function() {
                            jquery_894430ec1b1fa701650764dbf3b51b06c9011f10('html, body').animate({ scrollTop: 0 }, 'fast');
                        });
                    }
                }
            }
        }

        var buttons = [].slice.call(document.querySelectorAll("a[title^=regpopbox], button[title^=regpopbox]"));
        buttons.forEach(function(el) {
            var classes = el.className.replace(/[\t\r\n\f]/g, " ");
            if (classes.indexOf('wj-click-event') === -1) {
                var newClasses = classes + ' wj-click-event';
                el.className = newClasses;
                el.removeEventListener('click', onClickEvent);
                el.addEventListener('click', onClickEvent);
            }
        });
    });
}
document.body.appendChild(js_jquery_894430ec1b1fa701650764dbf3b51b06c9011f10);
js_jquery_894430ec1b1fa701650764dbf3b51b06c9011f10.src = 'https://cdnjs.cloudflare.com/ajax/libs/jquery/'+wj_jquery_version+'/jquery.min.js';
