<?php
/*
Author: Mark Lyndon B. Ramos a.k.a m10n6
ClassName: dbconHelper
Description: Database connection using mysqli and mysql for legacy servers
*/

class dbconHelper {
	private $dbname, $user, $pass, $host;//, $connection;

	function __construct($db, $user, $pass, $host){
		$this->dbname = $db;
		$this->user = $user;
		$this->pass = $pass;
		$this->host = $host;
	}

	function connect(){
		if(function_exists('mysqli_connect')){
		   	//Then do code
			$connect  = new mysqli($this->host, $this->user, $this->pass, $this->dbname);
			// Check connection
			if ($connect->connect_error) {
			    die("Connection failed: " . $connect->connect_error);
			} 
		}else{
		  	//legacy
			$connect  = mysql_pconnect($this->host, $this->user, $this->pass) or die(mysql_error());
			mysql_select_db($this->dbname, $connect) or die(mysql_error());
		}
		return $connect;
	}

	function dbquery($sql){
		$connect = $this->connect();
		// print_r($connect);
		// $this->display_result($sql);
		if(function_exists('mysqli_connect')){
		   	//Then do code
			$result = $connect->query($sql);

			if ($result->num_rows > 0) {
			    // output data of each row
			    while($row = $result->fetch_assoc()) {
			        $userdata['data'][] = json_encode($row); 
			    }
			    
			} else {
			    $userdata = false;
			}			
			// mysqli_free_result($result);

			if(strpos($sql, 'insert') === false){

			}else{
				$userdata = mysqli_insert_id($connect);
			}

			// $connect->close();
			// return $userdata;
		}else{
		  	//legacy
			$result  = mysql_query($sql);
			if(mysql_num_rows($result) > 0){
				while($row = mysql_fetch_assoc($result)){
					$userdata['data'][] = json_encode($row); 	
				}
			}else{
				$userdata = false;
			}
			mysql_free_result($result);

			if(strpos($sql, 'insert') === false){

			}else{
				$userdata = mysql_insert_id();
			}
		}		
		return json_encode($userdata);
		// return $userdata;
	}

	function dbSanitize($var){
		$connect = $this->connect();
		if(function_exists('mysqli_connect')){
			$sanitized = mysqli_real_escape_string($connect, $var);
		}else{
			$sanitized = mysql_escape_string($var);
		}
		return $sanitized;
	}

	function display_result($result){
		echo '<pre>';
		print_r($result);
		echo '</pre>';
	}
}

/*
USAGE

include_once('db.php');
  $hostname = "localhost";
  $database = "desktools";
  $username_r = "root";
  $password_r = 'root';
$conn = new dbconHelper($database, $username_r, $password_r, $hostname);
$conn->connect();
$res = $conn->dbquery("select * from `users`" );
$res = json_decode($res);
foreach ($res->data as $key) {
  # code...
  $nres = json_decode($key);
  echo $nres->email.'<br>';
}

// $res = $conn->dbquery("select * from `users` where `email` = 'ml13ramos@yahoo.com'" );
// $res = json_decode($res);
// $res = json_decode($res->data[0]);
// echo $res->email;

$conn->display_result($res);


*/

?>