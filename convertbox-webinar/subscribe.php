<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
// print_r($_POST);exit();
session_start();
$timezone = $_SESSION['time'];

//This script works with original EverWebinar setup with a JIT webinar using America/EST timezone.
//You'll need one fixed schedule added e.g. 1am, then JIT period you choose.
//It will add users to the next JIT webinar using the one-click link.


//These are the name/email variables from the 1-click URL.
//You may need to change the name of the variable below depending on how they look in your url.
//e.g. if your URL is www.site.com?name=bob&email=bob@gmail.com then this will work 'as is'.
//if your url is www.site.com?firstname=bob@contact=bob@gmail.com then you'll need to change the code below to be:
//$emailreg = urlencode($_GET["contact"]);
//$namereg = $_GET["firstname"];

// $emailreg = urlencode(urldecode($_GET["email"]));
$schedule = $_GET['webinar_dt'];
$emailreg = str_replace('+', '%2B', urlencode($_GET["email"]));
if(!empty($_GET["name"])){
	$namereg = urlencode(urldecode($_GET["name"]));	
}else{
	$namereg = 'No Name';
}

//This information is from EverWebinar - found in Step 5 - integrations ... API integrations

// $api_key = 'a4e79bf7eb640a9ddabfe3927b2826c7e0f91d1bee0c3149313fff6ddb1dff9f';
// $webinar_id = '7c7dc3dcf4';
$api_key = 'a4e79bf7eb640a9ddabfe3927b2826c7e0f91d1bee0c3149313fff6ddb1dff9f';
$webinar_id = 'fcf5f78383';


// $ch = curl_init();
// $header = array("User-Agent: " . $_SERVER["HTTP_USER_AGENT"], "Content-Type: application/x-www-form-urlencoded");

// $todayreg = date("Y-m-d");

// $url_register = 'https://webinarjam.genndi.com/api/everwebinar/register';
// $url_sched = 'https://webinarjam.genndi.com/api/everwebinar/webinar';

// curl_setopt($ch, CURLOPT_URL, $url_sched);
// curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
// curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
// curl_setopt($ch, CURLOPT_POST, 1);

// $timezone = $_GET['gmt_offset'];
// // $timezone = 'GMT+01:00';
// $len_timezone = strlen($timezone);
// // echo $len.'<br>';
// $timezone = substr($timezone, 0, ($len_timezone-2)).':'.substr($timezone, -2, $len_timezone);
// // curl_setopt($ch, CURLOPT_POSTFIELDS,
// //             "api_key=".$api_key."&webinar_id=".$webinar_id."&first_name=".$namereg."&email=".$emailreg."&schedule=".$schedule."&real_dates=1");
// curl_setopt($ch, CURLOPT_POSTFIELDS,
//             "api_key=".$api_key."&webinar_id=".$webinar_id."&timezone=".$timezone."&real_dates=1
//             ");//&selected_date=".$todayreg."&to_timezone=America/New_York&from_timezone=America/New_York&tz_selected=1");
// // curl_setopt($ch, CURLOPT_POSTFIELDS,
// //             "webicode=".$webicode."&memberid=".$memberid."&page=registration&page_tag=formregistration&userid=0&first_name=".$namereg."&email_address=".$emailreg."&selected_date=".$todayreg."&selected_schedule=jot&to_timezone=America/New_York&from_timezone=America/New_York&tz_selected=1");
			
// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

// $server_output = curl_exec ($ch);
// curl_close ($ch);
// $json_encoded_serveroutput = json_decode($server_output);
// // echo '<pre>';
// // print_r($json_encoded_serveroutput);
// // echo '</pre>';
// if($json_encoded_serveroutput->status == 'success'){

// 	foreach ($json_encoded_serveroutput->webinar->schedules as $sched) {
// 		# code...
// 	echo '<option value="'.$sched->schedule.'">'.$sched->date.'</option>';	
// 	}
// }




if(!empty($emailreg)){
  // sleep(10);
  // date_default_timezone_set('America/New_York');
  // $schedule = $_POST['webinar_dt'];
  $schedule = 'jot';

  $ch = curl_init();
  $header = array("User-Agent: " . $_SERVER["HTTP_USER_AGENT"], "Content-Type: application/x-www-form-urlencoded");

  $todayreg = date("Y-m-d");


  curl_setopt($ch, CURLOPT_URL,"https://webinarjam.genndi.com/api/everwebinar/register");
  curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_POST, 1);

  // curl_setopt($ch, CURLOPT_POSTFIELDS,
  //             "api_key=".$api_key."&webinar_id=".$webinar_id."&first_name=".$namereg."&email=".$emailreg."&schedule=".$schedule."&real_dates=1");
  curl_setopt($ch, CURLOPT_POSTFIELDS,
              "api_key=".$api_key."&webinar_id=".$webinar_id."&first_name=".$namereg."&email=".$emailreg."&real_dates=1&schedule=".$schedule);//&selected_date=".$todayreg."&to_timezone=America/New_York&from_timezone=America/New_York&tz_selected=1");
  // curl_setopt($ch, CURLOPT_POSTFIELDS,
  //             "webicode=".$webicode."&memberid=".$memberid."&page=registration&page_tag=formregistration&userid=0&first_name=".$namereg."&email_address=".$emailreg."&selected_date=".$todayreg."&selected_schedule=jot&to_timezone=America/New_York&from_timezone=America/New_York&tz_selected=1");
        
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

  $server_output = curl_exec ($ch);


  $pieces = explode(" ", $server_output);
  $pieces2 = explode("'", $server_output);

  // echo '<pre>';
  // print_r($pieces2);
  // // echo $schedule;
  // echo '</pre>';
  $result = json_decode($pieces2[0]);
  // print_r($result->user->live_room_url);

  $URLGO = $result->user->thank_you_url;
  // $URLGO = $pieces2[7];
  // echo $URLGO;
  curl_close ($ch);
  // echo $URLGO;
}

?>
<script> window.location = "<?php echo $URLGO; ?>"; </script>


