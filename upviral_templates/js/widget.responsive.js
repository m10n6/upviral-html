function upviral_widget_resizer(){
	$('.widget-giveaway-1').each(function(index){
		var widget_width = $(this).width();
		// var $this = $(this)
		if(widget_width <= 480){
			$(this).find('.ranknum').hide();	
			$(this).find('.points_count').hide();	
			$(this).find('.secondary_details').show();
			$(this).find('.email_text').attr('style','width: 100%; text-align:center;');
			$(this).find('.upviral_lb').attr('style', 'border-radius: 5px 5px 0px 0px;');

			$(this).find('.upviral_lb.darklb').attr('style', 'background-color: #1d232b !important; border-radius: 5px 5px 0px 0px;');
		}else{
			$(this).find('.ranknum').show();	
			$(this).find('.points_count').show();	
			$(this).find('.secondary_details').hide();
			$(this).find('.email_text').removeAttr('style');	
			$(this).find('.upviral_lb').removeAttr('style');	

			$(this).find('.upviral_lb.darklb').removeAttr('style');	
		}
		console.log(widget_width);	

	});

	$('.leader-board').each(function(index){
		var widget_width = $(this).width();
		// var $this = $(this)
		console.log(widget_width);
		// console.log($(this).find('.points_count').html());
		if(widget_width <= 480){
			$(this).find('.ranknum').hide();	
			$(this).find('.points_count').hide();	
			$(this).find('.secondary_details').show();
			$(this).find('.email_text').attr('style','width: 100%; text-align:center;');
			$(this).find('.upviral_lb').attr('style', 'border-radius: 5px 5px 0px 0px;');

			$(this).find('.upviral_lb.darklb').attr('style', 'background-color: #1d232b !important; border-radius: 5px 5px 0px 0px;');
		}else{
			$(this).find('.ranknum').show();	
			$(this).find('.points_count').show();	
			$(this).find('.secondary_details').hide();
			$(this).find('.email_text').removeAttr('style');	
			$(this).find('.upviral_lb').removeAttr('style');	

			$(this).find('.upviral_lb.darklb').removeAttr('style');	
		}

	});
}

$(document).ready(function(){
	// var widget_width = $('.widget-giveaway-1').width();
	upviral_widget_resizer();
});

$(window).resize(function(){
	upviral_widget_resizer();
});