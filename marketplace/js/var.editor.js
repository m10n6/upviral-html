/*
https://ourcodeworld.com/articles/read/189/how-to-create-a-file-and-generate-a-download-with-javascript-in-the-browser-without-a-server
*/
function downloadTxt(filename, text) {
  var element = document.createElement('a');
  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
  element.setAttribute('download', filename);

  element.style.display = 'none';
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
}

// Start file download.
// download("hello.txt","This is the content of my file :)");


$(document).ready(function(){
	var scrollPosition;
	function editVariables(){
		scrollPosition = $(window).scrollTop();
		$(document).scrollTop(0);
		$('#custom-search-overlay').removeClass('fadeOut').addClass('fadeIn');
		$('#custom-search-overlay').show();
	}

	$('.var_edit').click(function(){
		editVariables();
	});

	$('#close_search').click(function(){
		$(document).scrollTop(scrollPosition);
		$('#search_sequence').val('');
		$('#custom-search-overlay').removeClass('fadeIn').addClass('fadeOut');
		setTimeout(function(){
			$('#custom-search-overlay').hide();	
		}, 1000);
	});

	$('#btn-cancel').click(function(){
		$('#close_search').trigger('click');
	});

	String.prototype.replaceAll = function(search, replacement) {
	    var target = this;
	    return target.split(search).join(replacement);
	};

	$('#btn-save').click(function(){
		var mccounter = $('#mccounter').val();
		var escounter = $('#escounter').val();
		console.log(mccounter + 'sd');
		// var data_value = Array();

		// for(var i=0; i < mccounter; i++){
		// 	data_value[i] = $('#vars_'+i).val();
		// }
		console.log(escounter + 'hello');
		// console.log(data_value);
		for(var i1=0; i1 < escounter; i1++){
			var html_f = $('#orig_eseq_'+ i1).html() + '';
			console.log(html_f);
			// console.log($('#eseq_'+ i1).html());
			for(var i2=0; i2 < mccounter; i2++){
				// console.log(mccounter);
				console.log($('#ovars_'+i2).val() + '<=hello');
				console.log($('#vars_'+i2).val() + '<=world');
				var reps = $('#vars_'+i2).val();
				var serc = $('#ovars_'+i2).val()

				if(reps != ''){
					html_f = html_f.replaceAll(serc, reps);
					console.log(html_f);					
				}
			}
			console.log(html_f);
			$('#eseq_'+ i1).html(html_f);
		}

		$('.var_edit').bind('click', function(){editVariables()});
		$('#close_search').trigger('click');
	});

	$('#btn-download').click(function(){
		var filename = $(this).data('filename');
		var escounter = $('#escounter').val();
		var html_f = '';
		for(var i1=0; i1 < escounter; i1++){
			html_f += '' + $('#eseq_'+ i1).html() + '\n\r';
			html_f += '==============================================================================================\n\r';
		}

		$.ajax({
			type: "post",
			url : "stripper.php",
			data : {
				"html" : html_f
			},
			success: function(result){
				downloadTxt(filename, result);		
			}
		});
		
	});
});