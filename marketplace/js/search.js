$(document).ready(function(){
	var scrollPosition;
	$('#search-icon').click(function(){
		scrollPosition = $(window).scrollTop();
		$('#custom-search-overlay').removeClass('fadeOut').addClass('fadeIn');
		$('#custom-search-overlay').show();
		$('#search_sequence').focus();
	});

	$('#close_search').click(function(){
		$(document).scrollTop(scrollPosition);
		$('#search_sequence').val('');
		$('#custom-search-overlay').removeClass('fadeIn').addClass('fadeOut');
		setTimeout(function(){
			$('#custom-search-overlay').hide();	
		}, 1000);
		$('#search_sequence').blur();
		$('#search_result').html('');
	});

	$('#search_sequence').keyup(function(e){
		console.log($(this).val());
		var txt = $(this).val();
		console.log(txt.length);
		if(txt.length >= 4){
			$('#search_result').html('<h3 class="text-white">Searching...</h3>');
			$.ajax({
				type: 'post',
				url : 'search_sequence.php',
				data : {
					"search" : txt
				},
				success : function(result){
					$('#search_result').html(result);
				}
			});
		}else{
			$('#search_result').html('');
		}
	});
});