<?php
$location = $_POST['location'];

// $location = 'Amsterdam, Netherlands';

if(!empty($location)){
	$location = urlencode($location);
}

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL,"http://www.mapquestapi.com/geocoding/v1/address?key=ZmQ4HmQ1byKp6p8CelAWknuobmp2BYPE");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS,
            "inFormat=kvp&outFormat=json&thumbMaps=false&location={$location}");

// in real life you should use something like:
// curl_setopt($ch, CURLOPT_POSTFIELDS, 
//          http_build_query(array('postvar1' => 'value1')));

// receive server response ...
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$server_output = curl_exec ($ch);

curl_close ($ch);

echo $server_output;
// echo '<pre>';
// print_r($server_output);

?>