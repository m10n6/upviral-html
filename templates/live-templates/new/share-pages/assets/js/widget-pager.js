$(document).ready(function(){
	var leaderboards = $('#leaderboard-container .new-leaderboard-cont').length;
	console.log(leaderboards);

	var current_page = 1;
	for(var i = 1; i <= leaderboards; i++){
		$('#leaderboard-container .new-leaderboard-cont[data-page='+ (i+1) +']').hide();
	}

	$('.leaderboard-nav .left').click(function(){
		var pager = $('#leaderboard-container .new-leaderboard-cont[data-page='+ (current_page - 1) +']').data('seq');
		$('.leaderboard-nav .center').html(pager);

		if(current_page > 1 && current_page <= leaderboards){
			$('#leaderboard-container .new-leaderboard-cont[data-page='+ (current_page) +']').hide();	
			$('#leaderboard-container .new-leaderboard-cont[data-page='+ (current_page - 1) +']').show();	
			current_page--;			
		}


		if(current_page <= 1){
			$('.leaderboard-nav .left .nav').removeClass('inactive').addClass('inactive');
			$('.leaderboard-nav .right .nav').removeClass('inactive').addClass('active');
		}else{
			
		}
		console.log(current_page);
	});

	$('.leaderboard-nav .right').click(function(){
		var pager = $('#leaderboard-container .new-leaderboard-cont[data-page='+ (current_page + 1) +']').data('seq');
		$('.leaderboard-nav .center').html(pager);
		console.log(current_page);
		if(current_page >= 1 && current_page < leaderboards){
			$('#leaderboard-container .new-leaderboard-cont[data-page='+ (current_page + 1) +']').show();	
			$('#leaderboard-container .new-leaderboard-cont[data-page='+ (current_page) +']').hide();	
			$('.leaderboard-nav .nav').removeClass('inactive');

			current_page++;
		}
		if(current_page == leaderboards){
			$('.leaderboard-nav .right .nav').removeClass('inactive').addClass('inactive');
		}
		console.log(current_page);
	});
});