$(document).ready(function(){

	$('.upviral-rewards-progress').each(function(){
		var prog = $(this).data('progress');
		var color = $(this).data('barcolor');
		var border_radius = 'border-radius: 4px 0px 0px 4px;';
		if(prog == 100){
			border_radius = 'border-radius: 4px;';
		}
		$(this).html('<div style="background-color: '+ color +'; width: '+ prog +'%; height: 100%; '+ border_radius +'"></div>');
	});

});