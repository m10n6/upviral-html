//resive module
var cloned_global;
var cloned_ctr = 0;
var orig_feat_img = $('#feat-image').height();
var top_sec_height = $('#top-section').height();
var optinbox_height = $('#top-section #optin-box').height();
var optincont_height = $('#top-section #optin-container').height();

function resize_window_function(){
    console.log('orig_feat_img : ' + orig_feat_img );
    var feat_img = $('#feat-image');
    // var top_sec_height = $('#top-section').height();
    // var feat_img_height = (orig_feat_img + top_sec_height) - 300;
    var feat_img_height = optinbox_height + (optincont_height / 2);
    console.log(feat_img_height);
    if(feat_img.length > 0){
      //re-order
      // alert('div found');
      var w = $(window).width();
      console.log("window width = " + w);
      var cloned = feat_img.clone();
      if(w <= 768){
        console.log('hello');
        if(cloned_ctr == 0){
          console.log(cloned);
          $('<div id="feat-cloned">' + cloned[0].outerHTML + '</div>').insertBefore('#optin-container');
          cloned_ctr++;
        }else{
          $('#feat-cloned').show();  
        }

        $('#featured-container').hide();
        $('#top-section').css({ "background-size": "auto 70%", "background-position": "top center", "height" : "fit-content" });  
        $('#top-section .overlay').css({ "height": "70%"});
      }else{
        $('#featured-container').show();  
        $('#feat-cloned').hide();
        // $('#top-section').height(768);
        $('#top-section').css({ "background-size": "cover", "background-position": "center", "height" : "fit-content" });  
        $('#top-section .overlay').css({ "height": "100%"});
      } 
    }else{
      $('#featured-container').hide();
    }

}

$(document).ready(function(){
  resize_window_function();
});

$(window).resize(function(){
  resize_window_function();
});